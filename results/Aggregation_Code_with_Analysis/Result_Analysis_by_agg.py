#Importing libraries and Delaring input variables
cutoff_for_top_percentile=0.1
cutoff_for_pos_neg_class=0.55
cutff_for_pos_number_cases=0.5

bucket = 'rsci-data-ingest' #putting bucket name
prefix='inference_aggregation/19KDQ-coords/'#Prefix parameter is path excluding bucket name

import os
import pandas as pd
import boto3
import io

#function for those tiles whose occurence is more  than 50
def get_result1(mydf1,top_percentile_cutoff,pos_neg_cutoof,pos_case_cutoff,offset_list):
    ndc={}
    #val=list(set(mydf1['offset']))
    p_count={}
    for v in offset_list:
        arr=sorted(mydf1['probability'][mydf1['offset']==v].values,reverse=True)
        #ndcall[v]=arr
       #p_count={}[v]=(len([i for i in arr if i > 0.5]),len(arr))
        var=arr[:round(len(arr)*top_percentile_cutoff)]
        #print(var)
       # print(var)
        if len(var)>2:
            pos=[1 if i>pos_neg_cutoof else 0 for i in var]
            ndc[v]=1 if sum(pos)/len(pos)>pos_neg_cutoof else 0
    mmydf=pd.DataFrame([ndc]).transpose()
    mmydf['offset']=mmydf.index
    mmydf['Pos/Neg']=mmydf[0]
    return mmydf.loc[:,['offset','Pos/Neg']].reset_index(drop=True)#pd.DataFrame([ndc]).transpose()

#function for those tiles whose occurence is less than 50
def get_result2(mydf,pos_neg_cutoof,pos_case_cutoff,offset_list):
    ndf=mydf.loc[mydf['offset'].isin(offset_list),:]
    ndf['predicted']=[1 if i>pos_neg_cutoof else 0 for i in ndf['probability']]
    findf=ndf.groupby('offset').agg({'predicted': 'mean'})
    findf['offset']=findf.index
    findf['average_of_prediction']=findf['predicted']
    #findf['count']=mydf.groupby('offset').agg({'predicted': 'count'})
    findf['Pos/Neg']=[1 if i>pos_case_cutoff else 0 for i in findf['average_of_prediction']]
    return findf.loc[:,['offset','Pos/Neg']].reset_index(drop=True)


## Pulling data and consolidated for analysis
mybucket = boto3.resource('s3').Bucket(bucket)
s3 = boto3.client('s3')

agg_json_files=pd.DataFrame()
for obj in mybucket.objects.filter(Prefix=prefix):#Prefix parameter is path excluding bucket name
    s3_file_key=obj.key
    obj = s3.get_object(Bucket=bucket, Key=s3_file_key)
    initial_df = pd.read_csv(io.BytesIO(obj['Body'].read()))
    print(initial_df.shape)
    agg_json_files= pd.concat([agg_json_files,initial_df])
agg_json_files=agg_json_files.reset_index(drop=True)# consolidated files for analysis

mydf=agg_json_files.copy()
mydf['offset']=[i[72:][:-8] for i in mydf['inference_key']]
findf=mydf.groupby('offset').agg({'inference_key': 'count'})
less_count_off=findf.loc[findf['inference_key']<50,:].index
more_count_off=findf.loc[findf['inference_key']>=50,:].index

more_mydf=get_result1(mydf,cutoff_for_top_percentile,cutoff_for_pos_neg_class,cutff_for_pos_number_cases,more_count_off)
less_mydf=get_result2(mydf,cutoff_for_pos_neg_class,cutff_for_pos_number_cases,more_count_off)
final_df=pd.concat([less_mydf,more_mydf])
final_df.to_csv('/root/v9_and_v10_scoring/v9/19KDQ/at_0.55.csv')