import gc
import json
from intermediate_functions import download_tile_aws, all_tiles_validate, data_tile, jp2_to_gtiff_10, jp2_to_gtiff_60, jp2_to_gtiff_20, tiff2_raster, apply_logic, array2raster
import shutil
import osr
import os
import ogr
import gdal
from sentinelhub import opensearch, get_tile_info, AwsTileRequest, WebFeatureService, BBox, CRS, DataCollection, SHConfig
import sys
#import cartopy as cp
import earthpy.spatial as es
import earthpy.plot as ep
import earthpy as et
import matplotlib.pyplot as plt
from shapely.geometry import mapping
from rasterio.mask import mask
from rasterio.plot import show
from shapely import speedups
import georaster as gr
import sentinelhub
import numpy as np
import pandas as pd
import rasterio as rio
import rasterio
import geopandas as gpd

#startdate = "2016-01-01"
#enddate = "2021-04-01"

path = input("Enter consolidate Path: ")
startdate = input("Enter startdate format YYYY-MM-dd : ")
enddate = input("Enter enddate format YYYY-MM-dd : ")

'''10UED
10UFA
10UFB
10UFC
10UFU
10UFV
10UGV'''
#'19HDS', '19HDT'

tiles_list = ['10UFC']  # 10UED,2019-04-24,0

# tiles_list=['19HDE']#],'19HCD']#,'19HCC','19HCB','19HDD','19HDC','19HDB']
dataCoveragePercentage_condition = 80
cloudyPixelPercentage_condition = 50
snowCover_condition = 50

# If you disable speedups, you won't get the NULL Pointer Exception, because you don't use the C implementation, rather than the usual implementation.

speedups.disable()

os.chdir(path)
sys.path.append(path)

# shutil.disk_usage() method tells the disk usage statistics about the given path as a named tuple with the attributes total, used and free


def check_disk(Drive_letter, size):
    import shutil
    if shutil.disk_usage(Drive_letter+':')[2] < size*1024*1024*1024:
        input("Press the <ENTER> key to continue...")

# date list from start to end


dates_list = []
date_list = pd.date_range(startdate, enddate, freq='D')
for date in date_list:
    date = date.strftime("%Y-%m-%d")
    dates_list.append(date)
len(dates_list)


array_index = dict()
extn = {
    'tileinfo': '.json',
    'jp2': '.jp2',
    'tif_translate': 'gdal_translate.tif',
    'tif_index': 'index.tif',
    'tif_logic': 'logic.tif',
    'csv_index': 'index.csv',
    'png_stats': 'calctime.png'
}

dates_list = dates_list[dates_list.index('2019-04-03'):]
path_c = path
#     tile_name = tiles_list[0]
#     date = dates_list[0]
for tile_name in tiles_list:
    folpath = tile_name+' '+startdate[:7]+'_'+enddate[:7]
    if not os.path.exists(folpath):
        os.makedirs(folpath)
    path = path_c+'\\'+folpath
    for date in dates_list:
        tile_name = tile_name
        aws_index = 0
        string_name = tile_name+','+date+','+str(aws_index)
        filtered_data = path+'\\filtered_data'
        check_disk(path[0], 5)  # checking the disk space to continue
        tile_found, tile_origin_coordinates = download_tile_aws(
            tile_name, date, path, dataCoveragePercentage_condition, cloudyPixelPercentage_condition, snowCover_condition)
        if(tile_found == True):
            if(all_tiles_validate(filtered_data, string_name, tile_name, date) == True):
                tile_path = filtered_data+'\\'+string_name
                # shutil.copy2(tile_path+'/'+'B02-gdal_translate.tif',tile_path+'/'+'B02-gdal_translate_copy.tif')
                bands = data_tile(filtered_data, string_name)
                # print(tile_dataframe)
                # Code for converting all 10m jp2 bands itno Gtfiff and resizing it to 20M resoltuion
                # path of all 10M resolution
                path_10 = bands[bands['res'] == 10]['path'].values
                # name os all 10M resoltuion bands
                band_10 = bands[bands['res'] == 10]['band'].values
                for i in range(len(band_10)):
                    jp2_to_gtiff_10(band_10[i], path_10[i])

                # code for iteraring over all the bands of 20M resolution and converting it itno Gtiff
                path_20 = bands[bands['res'] == 20]['path'].values
                band_20 = bands[bands['res'] == 20]['band'].values

                path_60 = bands[bands['res'] == 60]['path'].values
                band_60 = bands[bands['res'] == 60]['band'].values
                for i in range(len(band_20)):
                    jp2_to_gtiff_20(band_20[i], path_20[i])

                for i in range(len(band_60)):
                    jp2_to_gtiff_60(band_60[i], path_60[i])
                # storing all band names and path names in band_all and path_all respectively
                path_all = bands['path'].values
                band_all = bands['band'].values

                # creating a dictionary for storing band name as key and its raster matrix as value
                r = dict()
                meta_data_raster = []
                for i in range(len(band_all)):
                    # storing band name as key and raster matrix as its value in r dictionary
                    r[band_all[i]], meta_data_raster_ = tiff2_raster(
                        band_all[i]+'-gdal_translate', path_all[i])
                    meta_data_raster.append(meta_data_raster_)

                logic = apply_logic(r)
                # rams index calculation
                # Discriminates for rams relative to hematite-goethite
                index = np.divide(r['B04'], r['B12'])
                print("Index Matrx: ", index)

                # find pixels that pass every condition
                valid_pixel = logic['t1'] & logic['t2'] & logic['t3'] & logic['t4'] & logic['t5'] & logic['t6']
                print("Valid Pixel matrix:", valid_pixel)

                print("Shape of Index matrix :", index.shape)
                print("Shape of valid pixel matrix", valid_pixel.shape)

                index = np.multiply(valid_pixel, index)
                print(np.sum(index))

                array_index[string_name] = index
                rasterOrigin = tile_origin_coordinates
                pixelWidth = 5490
                pixelHeight = 5490

                path_out_index = os.path.join(tile_path, 'index_file.tif')
                path_converted_index = os.path.join(
                    tile_path, 'index_file_converted.tif')
                meta_data_raster[0]['dtype'] = 'float64'
                # saving index file in the tif format !!
                with rio.open(path_out_index, 'w', **meta_data_raster[0]) as ff:
                    ff.write(index, 1)

                command = 'gdalwarp'+str(' "') + str(path_out_index)+str('" "') + str(
                    path_converted_index)+str('" ')+str('-t_srs EPSG:4326')
                os.system(command)
                gc.collect()
            else:
                print("Some bands are missing for the tile",
                      tile_name, "for the date", date)
                print("----Not proceeded--")
