
# !aws s3 cp 's3://rioml/9 June training model all 13/' '/home/ubuntu/data/tiles/9 june all 13 /' --recursive
import numpy as np
import pandas as pd
from os import path
from os import listdir
import tifffile as tiff
import cv2
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, Flatten, Dropout
from keras import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import keras.backend as K
# storing paths
data_path = '/home/ubuntu/data/tiles/9 june all 13 _for_cnn/'

# function that generates updated VGG model
def classification_model(num_chs, input_shape = (56, 56)):
    # loading the base VGG16 model from Keras
    
    input_layer = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # Creating Block 1
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(input_layer)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 2
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 3
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating FC block
    f_pass = Flatten()(f_pass)
    f_pass = Dense(2048, activation = 'relu')(f_pass)
    f_pass = Dropout(0.10)(f_pass)
    f_pass = Dense(512, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model(input_layer, f_pass, name = 'Updated_VGG_'+str(num_chs)+'c')
    # return model
    return final_model

# batch generator function for training and validation
all_train_imgs = listdir(data_path+'train/pos/') + listdir(data_path+'train/neg/')
train_idxs = np.array(range(len(all_train_imgs)))

all_val_imgs = listdir(data_path+'val/pos/') + listdir(data_path+'val/neg/')
val_idxs = np.array(range(len(all_val_imgs)))

# loading data function - based on train idxs
def load_train_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'train/pos/'+all_train_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'train/pos/'+all_train_imgs[index])
            temp_data = cv2.resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'train/neg/'+all_train_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'train/neg/'+all_train_imgs[index])
            temp_data = cv2.resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)

# loading data function - based on val idxs
def load_val_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'val/pos/'+all_val_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'val/pos/'+all_val_imgs[index])
            temp_data = cv2.resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'val/neg/'+all_val_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'val/neg/'+all_val_imgs[index])
            temp_data = cv2.resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)


# batch data generator - train
def generate_batch_train(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_train_data(batch, img_size)
                batch = []
                
# batch data generator - val
def generate_batch_val(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_val_data(batch, img_size)
                batch = []
            
# creating a data generator object
train_generator = generate_batch_train(train_idxs, (56,56), batch_size = 16)
val_generator = generate_batch_val(val_idxs, (56,56), batch_size = 3384)


# defining model architecture
K.clear_session()
updated_model = classification_model(num_chs = 13)
# checking model summary
updated_model.summary()

# callbacks
model_callbacks = [EarlyStopping(patience=5, verbose=1),
                   ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
                   ModelCheckpoint(data_path+'model_classification_test.h5', monitor = 'loss', verbose=1, save_best_only=True, save_weights_only=True)]

# tying to fit model
updated_model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# fitting model to test
updated_model.fit(train_generator, steps_per_epoch = 646, epochs=1, 
                  validation_data=val_generator, validation_steps=1, callbacks = model_callbacks)



# validating accuracy from model
valid_x, valid_y = load_val_data(val_idxs, (56, 56))
valid_pred = updated_model.predict(valid_x)
valid_pred_class = valid_pred > 0.5
valid_pred_class = valid_pred_class.astype(int)
from sklearn.metrics import accuracy_score, classification_report
model_accuracy = accuracy_score(valid_y, valid_pred_class)
print(classification_report(valid_y, valid_pred_class))


# Unseen Data Analysis
all_unseen_imgs = listdir(data_path+'unseen/pos/') + listdir(data_path+'unseen/neg/')
unseen_idxs = np.array(range(len(all_unseen_imgs)))

# loading data function - based on val idxs
def load_unseen_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'unseen/pos/'+all_unseen_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'unseen/pos/'+all_unseen_imgs[index])
            temp_data = cv2.resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'unseen/neg/'+all_unseen_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'unseen/neg/'+all_unseen_imgs[index])
            temp_data = cv2.resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)

# unseen accuracy from model
unseen_x, unseen_y = load_unseen_data(unseen_idxs, (56, 56))
unseen_pred = updated_model.predict(unseen_x)
unseen_pred_class = (unseen_pred > 0.5).astype(int)
from sklearn.metrics import accuracy_score, classification_report
model_acc_unseen = accuracy_score(unseen_y, unseen_pred_class)
print(classification_report(unseen_y, unseen_pred_class))

# outputting results from unseen data
unseen_res = pd.DataFrame(columns = ['Tile Name', 'Predicted Probability', 'True Class'])
unseen_res['Tile Name'] = all_unseen_imgs
unseen_res['Predicted Probability'] = unseen_pred
unseen_res['True Class'] = unseen_y
unseen_res.to_csv(data_path+'Unseen Results.csv', index = False)


# function that helps us get tile name from crop image name
def get_tile_name(img_name):
    return img_name.split(',')[0]
# function for analyzing results at tile level
def classify_tile(pred_img_name, pred_prob, pos_thresh = 0.5, tile_thresh = 0.5):
    tile_name = list(map(get_tile_name, pred_img_name))
    data = pd.DataFrame(zip(tile_name, np.squeeze(pred_prob)), columns = ['Tile Name', 'Pred Prob'])
    data['Pred Class'] = [1 if pred > pos_thresh else 0 for pred in pred_prob]
    grouped_res = data.groupby('Tile Name')['Pred Class'].mean().reset_index()
    grouped_res['Tile Pred'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['Pred Class']]
    return grouped_res

# testing tile predictions
tile_predictions = classify_tile(all_unseen_imgs, unseen_pred)




# In[]
from tifffile import imread
from skimage.transform import resize
## Pre processing the imput image Before fedding it to the model :
def pre_process(image_path):
    image=imread(image_path)
    #image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB) ## Converting the image into RGB format is the model was trained on RGB Fromat!
    image=resize(image, (56, 56,13))
    image=image.astype("float32")
    
   # mean = np.array([123.68, 116.779, 103.939], dtype="float32") 
   # image=image-mean 
    image=np.expand_dims(image, axis=0)
    return image


#model=load_model('20may_Sentinel_model--04.h5')#Sentinel_model#

##  label map is being generated from train_gen during training  time !!

label_map={'neg': 0,
 'pos': 1}
      
label_map =dict(map(reversed, label_map.items()))

def predict_image(image_path,model,topk=5):
        image=pre_process(image_path)
        pred=model.predict(image)[0]
        max_prob_class=np.argmax(pred)
        class_name=label_map[max_prob_class]
        topk_class=(-pred).argsort()[:topk]
        top_k_class_names=[label_map[i] for i in topk_class]
        return class_name,top_k_class_names,max_prob_class,pred
import os
img_path='/home/ubuntu/data/tiles/9 june all 13 _for_cnn/unseen/'
flname1=os.listdir(img_path+'/neg')
flname2=os.listdir(img_path+'/pos')
imagePaths1=[img_path+'/neg/'+i for i in flname1]
imagePaths2=[img_path+'/pos/'+i for i in flname2]
imagePaths=imagePaths1+imagePaths2
predicted=[]
probs=[]
print(len(imagePaths))
for f in imagePaths:
    print(imagePaths.index(f))
    class_name,top_k_class_names,max_prob_class,prob=predict_image(f, updated_model,topk=1)
    predicted.append(class_name)
    probs.append(prob)
    

