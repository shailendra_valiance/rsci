import os
import shutil

def make_dir(new_path_train,new_path_val,new_path_unseen):
    if not os.path.exists(new_path_train):
        os.makedirs(new_path_train)
    else:
        shutil.rmtree(new_path_train)           # Removes all the subdirectories!
        os.makedirs(new_path_train)
    if not os.path.exists(new_path_val):
        os.makedirs(new_path_val)
    else:
        shutil.rmtree(new_path_val)           # Removes all the subdirectories!
        os.makedirs(new_path_val)
    if not os.path.exists(new_path_unseen):
        os.makedirs(new_path_unseen)
    else:
        shutil.rmtree(new_path_unseen)           # Removes all the subdirectories!
        os.makedirs(new_path_unseen)
        
import split_folders
#split_folders.ratio('/home/ubuntu/png data/', output="/home/ubuntu/png data valid", seed=1000, ratio=(0.8,0.2) ) # default value
path= '/home/ubuntu/data/tiles/14 june all 13'#please remove last slash "/"
new_path=path.rsplit('/',1)[0]+'/'+path.split('/')[-1]+'_for_cnn'
f=os.listdir(path+'/pos')
print(len(f))
print("positive-->",set([i[13:18] for i in f]))
print(len(set([i[13:18] for i in f])))
f=os.listdir(path+'/neg')
print(len(f))
print("negative-->",set([i[13:18] for i in f]))
print(len(set([i[13:18] for i in f])))
## Splitting data into training validation and unseen
'''
data={'train_pos':[ '18KZG','10UCA', '18LTN', '19HCE', '19JFN', '10UCG', '19JEG', '10UCF', '21MWM', '19JCG', '19KEQ', '19KES', '19JDK', '19JFH', '19JCH', '19JCJ', '10TES', '10TFT', '19KER', '10UCE'],
    'val_pos':['19KDQ', '19TDL', '19HDE', '19JDG', '19JGK', '19JDM'],
    'unseen_pos':['19KBB', '19HCT', '21MWN'],
    'train_neg':['19GCJ', '19HDS', '19KGS', '19HDU', '19GCM', '19HBT', '19HFB', '19HBB', '19HEB', '19GBP', '19KFB', '19HBS', '19HBU', '19HFA', '19KEB', '19HGE', '20HLC', '19GCQ', '20HKK'],
    'val_neg':['19KGP', '19GCH', '19HED', '19KGQ', '19HEC', '19KDB'],
    'unseen_neg':['19GCK', '19HDV', '19HDT']}
'''
make_dir(new_path+'/train/pos',new_path+'/val/pos',new_path+'/unseen/pos')
make_dir(new_path+'/train/neg',new_path+'/val/neg',new_path+'/unseen/neg')
for i in data.keys():
    print(i)
    f=os.listdir(path+'/'+i.split('_')[1])
    files=[k for k in f if k[13:18] in data[i]]
    for fl in files:
        shutil.copy(path+'/'+i.split('_')[1]+'/'+fl, new_path+'/'+i.split('_')[0]+'/'+i.split('_')[1]+'/'+fl)    
f=os.listdir(new_path+'/train/pos')
print(len(f))
print("training positive-->",set([i[13:18] for i in f]))
f=os.listdir(new_path+'/train/neg')
print(len(f))
print("training negative-->",set([i[13:18] for i in f]))
