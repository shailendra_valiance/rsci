import numpy as np
from os import listdir
data_path = '/home/ubuntu/data/tiles/28 june all 13/'
positives = listdir(data_path+'pos/') 
negatives = listdir(data_path+'neg/') 


pos_tile_names = list(set(map(lambda x: x.split(',')[0].split('_')[-1] + x[-5], positives)))
neg_tile_names = list(set(map(lambda x: x.split(',')[0].split('_')[-1] + x[-5], negatives)))

np.random.seed(9)
np.random.shuffle(pos_tile_names)
pos_tile_names.remove('19KBB2')
pos_tile_names.append('19KBB2')
np.random.shuffle(neg_tile_names)


data = {'train_pos': pos_tile_names[:71],
        'val_pos': pos_tile_names[71:80],
        'unseen_pos': pos_tile_names[80:],
        'train_neg': neg_tile_names[:60],
        'val_neg': neg_tile_names[60:67],
        'unseen_neg': neg_tile_names[67:]}