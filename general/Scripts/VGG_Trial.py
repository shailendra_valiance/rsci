
# !aws s3 cp 's3://rioml/9 June training model all 13/' '/home/ubuntu/data/tiles/9 june all 13 /' --recursive
import numpy as np
from os import path
from os import listdir
import tifffile as tiff
import cv2
from keras.layers import Dense, Dropout
from keras.layers import Input, Conv2D
from keras import Model
from keras.applications.vgg16 import VGG16

# storing paths
data_path = '/home/ubuntu/data/tiles/9 june all 13 /'

# function that generates updated VGG model
def updated_VGG16(num_chs, layer_train = True):
    # loading the base VGG16 model from Keras
    vgg_model = VGG16(weights = 'imagenet')
    
    # extracting individual layers from vgg model layers
    model_layers = [layer for layer in vgg_model.layers]
    
    updated_input = Input(shape = (224,224,num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # updating first convolution layer to make sure it accepts n channel inputs
    f_pass = Conv2D(filters = model_layers[1].filters, kernel_size = model_layers[1].kernel_size, 
                    padding=model_layers[1].padding, activation = model_layers[1].activation)(updated_input)
    
    # forward pass through model
    # checking if output is desired at first conv layer
    
    for i in range(2, 21):
        model_layers[i].trainable = layer_train
        f_pass = model_layers[i](f_pass)
    # once forward pass is completed, we build and compile the model
    # add dense layers to model
    f_pass = Dense(2048, activation = 'relu')(f_pass)
    f_pass = Dropout(0.10)(f_pass)
    f_pass = Dense(512, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    updated_model = Model(updated_input, f_pass, name = 'Updated_VGG_'+str(num_chs)+'c')
    # return model
    return updated_model

# batch generator function for training
all_img_names = listdir(data_path+'Pos') + listdir(data_path+'Neg')
img_idxs = np.array(range(len(all_img_names)))

# loading data function - based on idxs
def load_data(batch_ids):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'Pos/'+all_img_names[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'Pos/'+all_img_names[index])
            temp_data = cv2.resize(temp_data, (224,224))
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'Neg/'+all_img_names[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'Neg/'+all_img_names[index])
            temp_data = cv2.resize(temp_data, (224,224))
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)


# batch data generator
def generate_batch(idxs, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_data(batch)
                batch = []
            
# creating a data generator object
train_generator = generate_batch(img_idxs, batch_size = 16)


# defining model architecture
updated_model = updated_VGG16(num_chs = 13)
# checking model summary
updated_model.summary()

# tying to fit model
updated_model.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# fitting model to test
updated_model.fit(train_generator, steps_per_epoch = 975, epochs=1)

z = train_data[0:15]




