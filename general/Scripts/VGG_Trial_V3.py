#%%
# !aws s3 cp 's3://rioml/9 June training model all 13/' '/home/ubuntu/data/tiles/9 june all 13 /' --recursive
import numpy as np
import pandas as pd
from os import path
from os import listdir
import tifffile as tiff
import matplotlib.pyplot as plt
# from cv2 import resize
from skimage.transform import resize
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, BatchNormalization, Flatten, Dropout
from keras import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import keras.backend as K
# storing paths
data_path = '/home/ubuntu/data/tiles/13 june all 13_for_cnn/'

# function that generates updated VGG model
def classification_model(num_chs, input_shape = (56, 56)):
    
    input_layer = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # Creating Block 1
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(input_layer)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 2
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 3
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating FC block
    f_pass = Flatten()(f_pass)
    f_pass = Dense(2048, activation = 'relu')(f_pass)
    f_pass = Dropout(0.10)(f_pass)
    f_pass = Dense(512, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model(input_layer, f_pass, name = 'Updated_VGG_'+str(num_chs)+'c')
    # return model
    return final_model

# classification model reference - https://arxiv.org/pdf/2105.04430v1.pdf
    
def classification_model2(num_chs, input_shape = (56, 56)):
    input_layer = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # block 1
    f_pass = Conv2D(filters = 16, kernel_size = (2,2), 
                    padding='same', activation = 'relu')(input_layer)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    f_pass = Dropout(0.2)(f_pass)
    # block 2
    f_pass = Conv2D(filters = 32, kernel_size = (2,2), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 32, kernel_size = (2,2), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    f_pass = Dropout(0.2)(f_pass)
    # block 3
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    f_pass = Dropout(0.2)(f_pass)
    # block 3
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    
    # FC block
    f_pass = Flatten()(f_pass)
    f_pass = Dense(128, activation = 'relu')(f_pass)
    f_pass = Dropout(0.2)(f_pass)
    f_pass = Dense(128, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model(input_layer, f_pass, name = 'ImageClassify_'+str(num_chs)+'c')
    # return model
    return final_model

# ref: Medium Data Science
def classification_model3(num_chs, input_shape = (56, 56)):
    input_layer = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # block 1
    f_pass = Conv2D(filters = 32, kernel_size = (1,1), 
                    padding='valid', activation = 'relu')(input_layer)
    f_pass = Dropout(0.25)(f_pass)
    f_pass = Conv2D(filters = 48, kernel_size = (1,1), 
                    padding='valid', activation = 'relu')(input_layer)
    f_pass = Dropout(0.25)(f_pass)
    f_pass = Flatten()(f_pass)
    f_pass = Dense(64, activation = 'relu')(f_pass)
    f_pass = Dropout(0.5)(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model(input_layer, f_pass, name = 'ImageClassify_'+str(num_chs)+'c')
    # return model
    return final_model

# loading data function - based on train idxs
def load_train_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'train/pos/'+all_train_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'train/pos/'+all_train_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'train/neg/'+all_train_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'train/neg/'+all_train_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)

# loading data function - based on val idxs
def load_val_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'val/pos/'+all_val_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'val/pos/'+all_val_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'val/neg/'+all_val_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'val/neg/'+all_val_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)


# batch data generator - train
def generate_batch_train(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_train_data(batch, img_size)
                batch = []
                
# batch data generator - val
def generate_batch_val(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_val_data(batch, img_size)
                batch = []


# loading data function - based on val idxs
def load_unseen_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'unseen/pos/'+all_unseen_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'unseen/pos/'+all_unseen_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'unseen/neg/'+all_unseen_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'unseen/neg/'+all_unseen_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)

# plotting trained model results
def plot_results(model, plot_type = 'Accuracy'):
    # evaluating model
    model_results = model.history.history
    val_loss=model_results['val_loss']
    train_loss=model_results['loss']
    val_acc=model_results['val_accuracy']
    train_acc=model_results['accuracy']
    if plot_type == 'Loss':
        # setting x axis as per number of epochs
        xc=range(len(model_results['loss']))
        # plotting loss
        plt.figure(1,figsize=(7,5))
        plt.plot(xc,train_loss)
        plt.plot(xc,val_loss)
        plt.xlabel('Number of Epochs')
        plt.ylabel('Loss')
        plt.title('Train vs Val Loss')
        plt.grid(True)
        plt.legend(['Train','Val'],loc=4)
        plt.style.use(['classic'])
    elif plot_type == 'Accuracy':
        # setting x axis as per number of epochs
        xc=range(len(model_results['accuracy']))
        # plotting accuracy
        plt.figure(1,figsize=(7,5))
        plt.plot(xc,train_acc)
        plt.plot(xc,val_acc)
        plt.xlabel('Number of Epochs')
        plt.ylabel('Accuracy')
        plt.title('Train vs Val Accuracy')
        plt.grid(True)
        plt.legend(['Train','Val'],loc=4)
        plt.style.use(['classic'])


# function that helps us get tile name from crop image name
def get_tile_name(img_name):
    return img_name.split(',')[0].split('_')[-1] + img_name.split('_')[-1][0]
# function for analyzing results at tile level
def classify_tile(pred_img_name, pred_prob, true_class, pos_thresh = 0.5, tile_thresh = 0.5):
    tile_name = list(map(get_tile_name, pred_img_name))
    data = pd.DataFrame(zip(tile_name, np.squeeze(pred_prob), np.squeeze(true_class)), columns = ['Tile Name', 'Pred Prob', 'True Class'])
    data['Pred Class'] = [1 if pred > pos_thresh else 0 for pred in pred_prob]
    grouped_res = data.groupby('Tile Name')['Pred Class', 'True Class'].mean().reset_index()
    grouped_res['Tile Pred'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['Pred Class']]
    grouped_res['Tile True Label'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['True Class']]
    return grouped_res


#%%
# using batch generator function for training and validation
all_train_imgs = listdir(data_path+'train/pos/') + listdir(data_path+'train/neg/')
train_idxs = np.array(range(len(all_train_imgs)))

all_val_imgs = listdir(data_path+'val/pos/') + listdir(data_path+'val/neg/')
val_idxs = np.array(range(len(all_val_imgs)))

            
# creating a data generator object
train_generator = generate_batch_train(train_idxs, (56,56), batch_size = 32)
val_generator = generate_batch_val(val_idxs, (56,56), batch_size = 1925)

#%%
# clearing model computation graph
K.clear_session()
model1 = classification_model(num_chs = 13, input_shape = (56, 56))
model2 = classification_model2(num_chs = 13, input_shape = (56, 56))
model3 = classification_model3(num_chs = 13, input_shape = (56, 56))
# checking model summary
model1.summary()
model2.summary()
model3.summary()

# callbacks
model_callbacks1 = [EarlyStopping(patience=10, verbose=1),
                   ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
                   ModelCheckpoint(data_path+'model_1_weights.h5', monitor = 'val_loss', verbose=1, save_best_only=True, save_weights_only=True)]


model_callbacks2 = [EarlyStopping(patience=10, verbose=1),
                   ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
                   ModelCheckpoint(data_path+'model_2_weights.h5', monitor = 'val_loss', verbose=1, save_best_only=True, save_weights_only=True)]

model_callbacks3 = [EarlyStopping(patience=10, verbose=1),
                   ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
                   ModelCheckpoint(data_path+'model_3_weights.h5', monitor = 'val_loss', verbose=1, save_best_only=True, save_weights_only=True)]


# Training Model 1 
model1.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# fitting model to test
model1.fit(train_generator, steps_per_epoch = 512, 
           epochs=15, validation_data=val_generator, validation_steps=1, 
           callbacks = model_callbacks1)
# model1.save_weights(data_path+'model_1_weights.h5')
model1.load_weights(data_path+'model_1_weights.h5')
# Training Model 2
model2.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# model2.load_weights(data_path+'model_2_weights.h5')
# fitting model to test
model2.fit(train_generator, steps_per_epoch = 858, epochs=15, 
                  validation_data=val_generator, validation_steps=1, 
                  callbacks = model_callbacks2)
# model2.save_weights(data_path+'model_2_weights.h5')

# Training Model 3
model3.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# model2.load_weights(data_path+'model_2_weights.h5')
# fitting model to test
model3.fit(train_generator, steps_per_epoch = 429, epochs=15, 
                  validation_data=val_generator, validation_steps=1, 
                  callbacks = model_callbacks3)


# Unseen Data Analysis
all_unseen_imgs = listdir(data_path+'unseen/pos/') + listdir(data_path+'unseen/neg/')
unseen_idxs = np.array(range(len(all_unseen_imgs)))

# training data accuracy from model
train_x, train_y = load_train_data(np.array(range(len(all_train_imgs))), (56, 56))
train_pred1 = model1.predict(train_x)
train_pred2 = model2.predict(train_x)

# validating accuracy from model
valid_x, valid_y = load_val_data(np.array(range(len(all_val_imgs))), (56, 56))
valid_pred1 = model1.predict(valid_x)
valid_pred2 = model2.predict(valid_x)

# unseen accuracy from model
unseen_x, unseen_y = load_unseen_data(unseen_idxs, (56, 56))
unseen_pred1 = model1.predict(unseen_x)
unseen_pred2 = model2.predict(unseen_x)
# unseen_pred_class = (unseen_pred > 0.5).astype(int)
# from sklearn.metrics import accuracy_score, classification_report
# model_acc_unseen = accuracy_score(unseen_y, unseen_pred_class)


# storing tile prediction results - Model 1
tile_pred_unseen1 = classify_tile(all_unseen_imgs, unseen_pred1, unseen_y, pos_thresh = 0.5, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)
tile_pred_train1 = classify_tile(all_train_imgs, train_pred1, train_y, pos_thresh = 0.5, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)
tile_pred_valid1 = classify_tile(all_val_imgs, valid_pred1, valid_y, pos_thresh = 0.5, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)

results_model1 = pd.concat([tile_pred_train1, tile_pred_valid1, tile_pred_unseen1], axis=1)
results_model1.to_excel('Model 1 S50 T50.xlsx', index = False)

# storing tile prediction results - Model 2
tile_pred_unseen2 = classify_tile(all_unseen_imgs, unseen_pred2, unseen_y, pos_thresh = 0.5, tile_thresh = 0.5)
tile_pred_train2 = classify_tile(all_train_imgs, train_pred2, train_y, pos_thresh = 0.5, tile_thresh = 0.5)
tile_pred_valid2 = classify_tile(all_val_imgs, valid_pred2, valid_y, pos_thresh = 0.5, tile_thresh = 0.5)

# save results
tile_pred_unseen1.to_excel('Unseen Data 1 - 35.xlsx',index = False)
tile_pred_train1.to_excel('Train Data 1 - 35.xlsx',index = False)
tile_pred_valid1.to_excel('Valid Data 1 - 35.xlsx',index = False)

tile_pred_unseen2.to_excel('Unseen Data 2 - 5.xlsx',index = False)
tile_pred_train2.to_excel('Train Data 2 - 5.xlsx',index = False)
tile_pred_valid2.to_excel('Valid Data 2 - 5.xlsx',index = False)

# class weights calculation
from sklearn.utils import class_weight
class_weights = class_weight.compute_class_weight('balanced', 
                                                  np.unique(train_y), train_y)
# convert to dictionary for keras
class_weights = dict(enumerate(class_weights))


# scoring new test images
test_path = '/home/ubuntu/data/tiles/19 june all 13'
def load_test_data(test_path, img_size):
    x = []
    test_names = listdir(test_path)
    for img_name in test_names:
        temp_data = tiff.imread(test_path+'/'+img_name)
        temp_data = resize(temp_data, img_size)
        # temp_data = np.expand_dims(temp_data, axis = 0)
        x.append(temp_data)
    return np.array(x), test_names

test_data, test_names = load_test_data(test_path, (56,56))
test_pred = model1.predict(test_data)

test_results = pd.DataFrame(columns = ['image_name', 'probability'])
test_results['image_name'] = test_names
test_results['probability'] = test_pred
test_results['pred_class_0.4'] = ['Positive' if pred > 0.4 else 'Negative' for pred in test_pred]
test_results['pred_class_0.5'] = ['Positive' if pred > 0.5 else 'Negative' for pred in test_pred]
test_results['pred_class_0.6'] = ['Positive' if pred > 0.6 else 'Negative' for pred in test_pred]
test_results.to_excel('Model Inference - 19 June.xlsx', index = False)


