# CLI command to upload data from local to aws bucket
# aws s3 cp 'C:\Ashish_consolidate\' 's3://rioml/tiles/' --recursive
import boto3
import pandas as pd
import gdal
from PIL import Image
import rasterio
import shutil
import gc
import os
import numpy as np
from affine import Affine
from pyproj import Proj, transform
import random
from rasterio.windows import Window
import warnings
warnings.filterwarnings("ignore")

path = input("Destination path : ")
csv_path = input("csv Path : ")
neg = pd.read_csv(csv_path)
pos = pd.read_csv(csv_path)

tiles = list(pos['FullGrid'])+list(neg['FullGrid'])


def reading_points(path):
    x = pos['X'][pos['FullGrid'] == path.split()[0][-5:]
                 ].reset_index(drop=True)
    y = pos['Y'][pos['FullGrid'] == path.split()[0][-5:]
                 ].reset_index(drop=True)
    sam = 'Pos'
    if len(x) == 0 and len(y) == 0:
        x = neg['X'][neg['FullGrid'] == path.split()[0][-5:]
                     ].reset_index(drop=True)
        y = neg['Y'][neg['FullGrid'] == path.split()[0][-5:]
                     ].reset_index(drop=True)
        sam = 'Neg'
    print(x, y, sam)
    return x, y, sam


def getting_pixel_points(tif_path, lon_t, lat_t):
    with rasterio.open(tif_path) as r:
        T0 = r.transform  # upper-left pixel corner affine transform
        p1 = Proj(r.crs)
        A = r.read()  # pixel values

    # All rows and columns
    cols, rows = np.meshgrid(np.arange(A.shape[2]), np.arange(A.shape[1]))

    # Get affine transform for pixel centres
    T1 = T0 * Affine.translation(0.5, 0.5)
    # Function to convert pixel row/column index (from 0) to easting/northing at centre
    def rc2en(r, c): return (c, r) * T1

    # All eastings and northings (there is probably a faster way to do this)
    eastings, northings = np.vectorize(
        rc2en, otypes=[np.float, np.float])(rows, cols)

    # Project all longitudes, latitudes
    p2 = Proj(proj='latlong', datum='WGS84')
    longs, lats = transform(p1, p2, eastings, northings)

    all_i = []
    all_j = []
    all_d = []
    for i in range(longs.shape[0]):
        # print(i)
        for j in range(lats.shape[1]):
            all_i.append(i)
            all_j.append(j)
            all_d.append(((longs[i][j]-lon_t)**2+(lats[i][j]-lat_t)**2)**0.5)
    point_a = all_i[all_d.index(min(all_d))]
    point_b = all_j[all_d.index(min(all_d))]
    # print(point_a,point_b)
    return (point_a, point_b)


def cropping_around_point(tif_path, point_a, point_b, km_list, n):
    with rasterio.open(tif_path) as src:
        # The size in pixels of your desired window
        # xsize, ysize = src.width/2), int(src.height/2)
        # Create a Window and calculate the transform from the source dataset
        #    window = Window(xoff, yoff, xsize, ysize)
        #    window = Window(int(src.width/2-(src.width/100)*length), int(src.width/2+(src.width/100)*length), int(src.width/2-(src.height/100)*length), int(src.width/2+(src.height/100)*length))
        for km in km_list:
            window = Window(int(point_a-src.width/100*km/2),
                            int(point_b-src.height/100*km/2),
                            int(src.width/100*km),
                            int(src.height/100*km))

            transformed = src.window_transform(window)

            # Create a new cropped raster to write to
            profile = src.profile
            profile.update({
                'height': int(src.height/100*km),
                'width': int(src.width/100*km),
                'transform': transformed})

#            print(src.height,src.width)
            filename = tif_path[:len(tif_path)-4]+'_'+str(km) + \
                'x'+str(km)+'_cropped_'+str(n)+tif_path[-4:]
            with rasterio.open(filename, 'w', **profile) as dst:
                # Read the data from the window and write it to the output raster
                dst.write(src.read(window=window))
    return (filename).rsplit('/', 1)[0], (filename).rsplit('/', 1)[1]
    # return (point_a,point_b,src.width,src.height)


def check_disk(Drive_letter, size):
    import shutil
    if shutil.disk_usage(Drive_letter+':')[2] < size*1024*1024*1024:
        input("Press the <ENTER> key to continue...")


# assumes credentials & configuration are handled outside python in .aws directory or environment variables
s3 = boto3.resource('s3')


def download_s3_folder(bucket_name, s3_folder, local_dir=None):
    bucket = s3.Bucket(bucket_name)
    for obj in bucket.objects.filter(Prefix=s3_folder):
        if obj.key[-4:] == '.tif' and obj.key.rsplit('/', 1)[-1][:3] in ['B01', 'B02', 'B03', 'B04', 'B05', 'B06', 'B07', 'B08', 'B09', 'B10', 'B11', 'B12', 'B8A']:
            # print('c')
            target = obj.key if local_dir is None \
                else os.path.join(local_dir, os.path.relpath(obj.key, s3_folder))
            if 'trans.tif' in target:
                break
            if not os.path.exists(os.path.dirname(target)):
                os.makedirs(os.path.dirname(target))
            if obj.key[-1] == '/':
                continue
            bucket.download_file(obj.key, target)

# In[]


tiles = sorted(list(set(tiles)))
len(tiles)
# tiles[100:200].index('21MWN')

# tiles=tiles[:100]
# tiles=['22MFT']#=tiles[100:]


km_list = [1]
not_downloaded, incomplete_downloaded = [], []
len(tiles)
# tiles=tiles[tiles.index(tile):]
for tile in tiles:
    print(tiles.index(tile), tile)
#    if tile in lst:
#        print('already exist-->',tile)
#        continue
#    check_disk('E',5)
    new_path = path+'/'+tile+' 2016-01_2021-04/filtered_data'
    if not os.path.exists(new_path):
        os.makedirs(new_path)
    #'tiles/21LYJ 2016-01_2021-04/filtered_data/'
    download_s3_folder('rioml', new_path.split('/', 4)[-1]+'/', new_path)
    if len(os.listdir(new_path)) == 0:
        print('not_downloaded-->'+tile, not_downloaded.append(tile))
        shutil.rmtree(path+'/'+tile+' 2016-01_2021-04')
        continue
    if len(os.listdir(new_path)) < 5 and len(os.listdir(new_path)) > 0:
        print('incomplete_downloaded-->'+tile,
              incomplete_downloaded.append(tile))
        shutil.rmtree(path+'/'+tile+' 2016-01_2021-04')
        continue
    print('Downloaded-->', tile)
    #download_s3_folder('rioml', 'tiles/21LYJ 2016-01_2021-04/filtered_data/',new_path)
    folder = sorted(os.listdir(new_path))
    sr = 0
    my_dict = {}
    for tf in folder:
        tif_path = new_path+'/'+tf
        tiffs = sorted(os.listdir(tif_path))
        if len(tiffs) == 13:
            tiffs = [i for i in tiffs if i[:8] in ['B01-gdal', 'B02-gdal', 'B03-gdal', 'B04-gdal', 'B05-gdal',
                                                   'B06-gdal', 'B07-gdal', 'B08-gdal', 'B09-gdal', 'B10-gdal', 'B11-gdal', 'B12-gdal', 'B8A-gdal']]
            # os.chdir(tif_path)
            command = 'gdalbuildvrt -separate "'+tif_path+'/stack.vrt" "' + tif_path+'/'+tiffs[0]+'" "'+tif_path+'/'+tiffs[1]+'" "'+tif_path+'/'+tiffs[2]+'" "'+tif_path+'/'+tiffs[3]+'" "'+tif_path+'/'+tiffs[4]+'" "'+tif_path + \
                '/'+tiffs[5]+'" "'+tif_path+'/'+tiffs[6]+'" "'+tif_path+'/'+tiffs[7]+'" "'+tif_path+'/'+tiffs[8]+'" "' + \
                tif_path+'/'+tiffs[9]+'" "'+tif_path+'/'+tiffs[10]+'" "' + \
                tif_path+'/'+tiffs[11]+'" "'+tif_path+'/'+tiffs[12]+'"'
            #command='gdalbuildvrt -separate stack.vrt "'+tiffs[0]+'" "'+tiffs[1]+'" "'+tiffs[2]+'"'
            # command="gdalbuildvrt -separate stack.vrt "+ str(path[0])+str(path[1])+str(path[2])#+str(path[3])+str(path[4])+str(path[5])+str(path[6])+str(path[7])+str(path[8])+str(path[9])+str(path[10])+str(path[11])+str(path[12])
            print("##", os.system(command))
            # naming the output path and output file name
            output_file_name = 'all_13_bands_'+tf.rsplit(',', 1)[0]+".tif"
            # output_file_path=os.path.join(output_path,output_file_name)
            # saving it int tif format
            command_2 = 'gdal_translate "'+tif_path+'/stack.vrt" "' + \
                tif_path+'/'+str(output_file_name)+'"'
            print("**", os.system(command_2))
            os.remove(tif_path+'/stack.vrt')
            X, Y, sign = reading_points(tif_path+'/'+output_file_name)
            comb_tif_path = path+'/'+sign+'_combined13_tif'
            if not os.path.exists(comb_tif_path):
                os.makedirs(comb_tif_path)
            for pnt in range(len(X)):
                lat, long = X[pnt], Y[pnt]
                # print(lat,long)

                if sr == 0:
                    try:
                        my_dict['point_a'+str(pnt)], my_dict['point_b'+str(pnt)] = getting_pixel_points(
                            tif_path+'/'+output_file_name, lat, long)
                        a10_b10 = True
                        print(my_dict['point_a'+str(pnt)],
                              my_dict['point_b'+str(pnt)])
                    except IndexError:
                        a10_b10 = False
                        pass
                    print('points have been calculated')
                if a10_b10 == True:
                    cropped_file_path, cropped_file_name = cropping_around_point(
                        tif_path+'/'+output_file_name, my_dict['point_a'+str(pnt)], my_dict['point_b'+str(pnt)], km_list, str(pnt+1))
                    shutil.move(cropped_file_path+'/'+cropped_file_name,
                                comb_tif_path+'/'+cropped_file_name)  # moving cropped file
                gc.collect()
                print('completed==='+output_file_name[12:])
            sr = sr+1
#            shutil.move(tif_path+'/'+output_file_name,comb_tif_path+'/'+output_file_name)
            os.remove(tif_path+'/'+output_file_name)
    shutil.rmtree(path+'/'+tile+' 2016-01_2021-04')


# In[]Experimental code Bellow
nle = []
bucket = s3.Bucket('rioml')
for obj in bucket.objects.filter(Prefix='tiles/'):
    nle.append(obj.key[:11])
# In[]

lst = os.listdir('/home/ubuntu/data/tiles/Pos_combined13_tif') + \
    os.listdir('/home/ubuntu/data/tiles/Neg_combined13_tif')
lst = [i[13:18] for i in lst]
lst = list(set(lst))
print(len(lst))
