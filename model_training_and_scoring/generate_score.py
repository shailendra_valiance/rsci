#%%
## load model
import numpy as np
import pandas as pd
from os import listdir
import tifffile as tiff
# from cv2 import resize
from skimage.transform import resize
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, BatchNormalization, Flatten, Dropout
from keras import Model


# function that generates updated VGG model
def classification_model(num_chs, input_shape = (56, 56)):
    
    input_layer = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # Creating Block 1
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(input_layer)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 2
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 3
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating FC block
    f_pass = Flatten()(f_pass)
    f_pass = Dense(2048, activation = 'relu')(f_pass)
    f_pass = Dropout(0.10)(f_pass)
    f_pass = Dense(512, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model(input_layer, f_pass, name = 'Modified_'+str(num_chs)+'c')
    # return model
    return final_model
#%%

def generate_results(sample_path, img_size, model):
    all_folders = listdir(sample_path)
    results = pd.DataFrame()
    for foldr in all_folders:
        all_imgs = listdir(test_path+'/'+foldr)
        all_imgs = [img for img in all_imgs if len(img) > 72]
        col_names = [] 
        x = []
        for img in all_imgs:
            temp_data = tiff.imread(test_path+'/'+foldr+'/'+img)
            temp_data = resize(temp_data, img_size)
            file_name = img.split(',')[0] + '_' + img.split(',')[1].split('_')[1] + '_' + img.split(',')[1].split('_')[2] 
            col_names.append(file_name)
            x.append(temp_data)
        foldr_data = np.array(x)
        foldr_preds = model1.predict(foldr_data)
        temp_results = pd.DataFrame(np.transpose(foldr_preds), columns = col_names)
        temp_results['Folder_Name'] = foldr
        results = pd.concat([results, temp_results], axis = 0, sort = True)
        print("Processed Folder:", foldr)
    return results

data_path = '/home/ubuntu/data/'
# define model
model1 = classification_model(num_chs = 13, input_shape = (56, 56))

# define model weights
model1.load_weights(data_path+'model_1_weights.h5')

# test path is folder having scoring files
test_path = '/home/ubuntu/data/19KBB'  
pred_scores = generate_results(test_path, (56, 56), model1)

# save the pred_scores folder to desired path
pred_scores.to_excel('Scoring Results.xlsx', index = False)

# looped results
img_path = '/home/ubuntu/data/19KBB/'
excel_path = '/home/ubuntu/data/Excel_Results/'

all_folders = listdir(img_path)
results = pd.DataFrame()
# code break on idx 65 of all_folders, continuing from 66
for foldr in all_folders[:1]:
    all_imgs = listdir(img_path+'/'+foldr)
    all_imgs = [img for img in all_imgs if len(img) > 72]
    if len(all_imgs) > 10201:
        continue
    col_names = [] 
    x = []
    for img in all_imgs:
        temp_data = tiff.imread(img_path+'/'+foldr+'/'+img)
        temp_data = resize(temp_data, (56,56))
        file_name = img.split(',')[0] + '_' + img.split(',')[1].split('_')[1] + '_' + img.split(',')[1].split('_')[2] 
        col_names.append(file_name)
        x.append(temp_data)
    foldr_data = np.array(x)
    foldr_preds = model1.predict(foldr_data)
    temp_results = pd.DataFrame(np.transpose(foldr_preds), columns = col_names)
    temp_results['Folder_Name'] = foldr
    # temp_results.to_excel('check.xlsx', index = False)
    temp_results.to_excel(excel_path+foldr+'.xlsx', index = False)
    results = pd.concat([results, temp_results], axis = 0, sort = True)
    results.to_excel(data_path+'results.xlsx', index = False)
    print("Processed Folder:", foldr)




