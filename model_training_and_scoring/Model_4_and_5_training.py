# loading packages
import numpy as np
import pandas as pd
from os import path
from os import listdir
import tifffile as tiff
import matplotlib.pyplot as plt
# from cv2 import resize
from skimage.transform import resize
from keras.layers import Input, Conv2D, MaxPooling2D, Dense, Concatenate, BatchNormalization, Flatten, Dropout
from keras import Model
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
import keras.backend as K
from math import ceil

# MODEL 4

# function that generates updated VGG model
def classification_model4(num_chs, input_shape = (56, 56)):
    
    input_layer = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    # Creating Block 1
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(input_layer)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 2
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 3
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating FC block
    f_pass = Flatten()(f_pass)
    f_pass = Dense(2048, activation = 'relu')(f_pass)
    f_pass = Dropout(0.10)(f_pass)
    f_pass = Dense(512, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model(input_layer, f_pass, name = 'Updated_VGG_'+str(num_chs)+'c')
    # return model
    return final_model

# loading data function - based on train idxs
def load_train_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'train/pos/'+all_train_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'train/pos/'+all_train_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'train/neg/'+all_train_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'train/neg/'+all_train_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)

# loading data function - based on val idxs
def load_val_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'val/pos/'+all_val_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'val/pos/'+all_val_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'val/neg/'+all_val_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'val/neg/'+all_val_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)


# batch data generator - train
def generate_batch_train(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_train_data(batch, img_size)
                batch = []
                
# batch data generator - val
def generate_batch_val(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_val_data(batch, img_size)
                batch = []


# loading data function - based on val idxs
def load_unseen_data(batch_ids, img_size):
    x = []
    y = []
    for index in batch_ids:
        if path.exists(data_path+'unseen/pos/'+all_unseen_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'unseen/pos/'+all_unseen_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
        elif path.exists(data_path+'unseen/neg/'+all_unseen_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'unseen/neg/'+all_unseen_imgs[index])
            temp_data = resize(temp_data, img_size)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
    return np.array(x), np.array(y)

# plotting trained model results
def plot_results(model, plot_type = 'Accuracy'):
    # evaluating model
    model_results = model.history.history
    val_loss=model_results['val_loss']
    train_loss=model_results['loss']
    val_acc=model_results['val_accuracy']
    train_acc=model_results['accuracy']
    if plot_type == 'Loss':
        # setting x axis as per number of epochs
        xc=range(len(model_results['loss']))
        # plotting loss
        plt.figure(1,figsize=(7,5))
        plt.plot(xc,train_loss)
        plt.plot(xc,val_loss)
        plt.xlabel('Number of Epochs')
        plt.ylabel('Loss')
        plt.title('Train vs Val Loss')
        plt.grid(True)
        plt.legend(['Train','Val'],loc=4)
        plt.style.use(['classic'])
    elif plot_type == 'Accuracy':
        # setting x axis as per number of epochs
        xc=range(len(model_results['accuracy']))
        # plotting accuracy
        plt.figure(1,figsize=(7,5))
        plt.plot(xc,train_acc)
        plt.plot(xc,val_acc)
        plt.xlabel('Number of Epochs')
        plt.ylabel('Accuracy')
        plt.title('Train vs Val Accuracy')
        plt.grid(True)
        plt.legend(['Train','Val'],loc=4)
        plt.style.use(['classic'])


# function that helps us get tile name from crop image name
def get_tile_name(img_name):
    return img_name.split(',')[0].split('_')[-1] + img_name.split('_')[-1][0]
# function for analyzing results at tile level
def classify_tile(pred_img_name, pred_prob, true_class, pos_thresh = 0.5, tile_thresh = 0.5):
    tile_name = list(map(get_tile_name, pred_img_name))
    data = pd.DataFrame(zip(tile_name, np.squeeze(pred_prob), np.squeeze(true_class)), columns = ['Tile Name', 'Pred Prob', 'True Class'])
    data['Pred Class'] = [1 if pred > pos_thresh else 0 for pred in pred_prob]
    grouped_res = data.groupby('Tile Name')['Pred Class', 'True Class'].mean().reset_index()
    grouped_res['Tile Pred'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['Pred Class']]
    grouped_res['Tile True Label'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['True Class']]
    return grouped_res


# data path
data_path = '/home/ubuntu/data/tiles/13 june all 13_for_cnn/'

# using batch generator function for training and validation
all_train_imgs = listdir(data_path+'train/pos/') + listdir(data_path+'train/neg/')
train_idxs = np.array(range(len(all_train_imgs)))

all_val_imgs = listdir(data_path+'val/pos/') + listdir(data_path+'val/neg/')
val_idxs = np.array(range(len(all_val_imgs)))

            
# creating a data generator object
train_generator = generate_batch_train(train_idxs, (56,56), batch_size = 32)
val_generator = generate_batch_val(val_idxs, (56,56), batch_size = 1925)


# creating model
K.clear_session()
model4 = classification_model4(num_chs = 13, input_shape = (56, 56))
model4.summary()


# training model
model_callbacks4 = [EarlyStopping(patience=10, verbose=1),
                   ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
                   ModelCheckpoint(data_path+'model_4_weights.h5', monitor = 'val_loss', verbose=1, save_best_only=True, save_weights_only=True)]

# Training Model 14
model4.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
# fitting model to test
model4.fit(train_generator, steps_per_epoch = 512, 
           epochs=15, validation_data=val_generator, validation_steps=1, 
           callbacks = model_callbacks4)

# PREDICTIONS
# using trained model for predictions; do not train and uncomment and load weights file
model4.load_weights(data_path+'model_4_weights.h5')

# Unseen Data Analysis
all_unseen_imgs = listdir(data_path+'unseen/pos/') + listdir(data_path+'unseen/neg/')
unseen_idxs = np.array(range(len(all_unseen_imgs)))

# training data accuracy from model
train_x, train_y = load_train_data(np.array(range(len(all_train_imgs))), (56, 56))
train_pred4 = model4.predict(train_x)


# validating accuracy from model
valid_x, valid_y = load_val_data(np.array(range(len(all_val_imgs))), (56, 56))
valid_pred4 = model4.predict(valid_x)


# unseen accuracy from model
unseen_x, unseen_y = load_unseen_data(unseen_idxs, (56, 56))
unseen_pred4 = model4.predict(unseen_x)

# storing tile prediction results - Model 1
tile_pred_unseen4 = classify_tile(all_unseen_imgs, unseen_pred4, unseen_y, pos_thresh = 0.5, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)
tile_pred_train4 = classify_tile(all_train_imgs, train_pred4, train_y, pos_thresh = 0.5, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)
tile_pred_valid4 = classify_tile(all_val_imgs, valid_pred4, valid_y, pos_thresh = 0.5, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)

results_model4 = pd.concat([tile_pred_train4, tile_pred_valid4, tile_pred_unseen4], axis=1)
results_model4.to_excel('Model 4 - S50 T50.xlsx', index = False)



# MODEL 5 - Includes Band Ratio
def classification_model5(num_chs, input_shape = (56, 56)):
    # input layer 1 is 13 band input
    input_layer_1 = Input(shape = (input_shape[0], input_shape[1], num_chs), name = 'Input_Layer_'+str(num_chs)+'c')
    input_layer_2 = Input(shape = (input_shape[0]*input_shape[1],), name = 'Input_Layer_band_ratio')
    
    # Creating Block 1
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(input_layer_1)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 64, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 2
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 128, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating Block 3
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = Conv2D(filters = 256, kernel_size = (3,3), 
                    padding='same', activation = 'relu')(f_pass)
    f_pass = BatchNormalization()(f_pass)
    f_pass = MaxPooling2D(pool_size = (2,2), strides = (2,2), padding = 'valid')(f_pass)
    # Creating FC block
    f_pass = Flatten()(f_pass)
    # adding band ratio input - input 2
    f_pass = Concatenate()([f_pass, input_layer_2])
    # fully connected layers
    f_pass = Dense(2048, activation = 'relu')(f_pass)
    f_pass = Dropout(0.10)(f_pass)
    f_pass = Dense(512, activation = 'relu')(f_pass)
    f_pass = Dense(1, activation='sigmoid')(f_pass)
    
    final_model = Model([input_layer_1, input_layer_2], f_pass, name = 'Custom_Model_'+str(num_chs)+'c')
    # return model
    return final_model


# data loading funtions

def load_train_data(batch_ids, img_size):
    x = []
    y = []
    ratio = []
    for index in batch_ids:
        if path.exists(data_path+'train/pos/'+all_train_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'train/pos/'+all_train_imgs[index])
            temp_data = resize(temp_data, img_size)
            temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
            temp_ratio = np.ravel(temp_ratio)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
            ratio.append(temp_ratio)
        elif path.exists(data_path+'train/neg/'+all_train_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'train/neg/'+all_train_imgs[index])
            temp_data = resize(temp_data, img_size)
            temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
            temp_ratio = np.ravel(temp_ratio)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
            ratio.append(temp_ratio)
    return [np.array(x), np.array(ratio)], np.array(y)

# loading data function - based on val idxs
def load_val_data(batch_ids, img_size):
    x = []
    y = []
    ratio = []
    for index in batch_ids:
        if path.exists(data_path+'val/pos/'+all_val_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'val/pos/'+all_val_imgs[index])
            temp_data = resize(temp_data, img_size)
            temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
            temp_ratio = np.ravel(temp_ratio)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
            ratio.append(temp_ratio)
        elif path.exists(data_path+'val/neg/'+all_val_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'val/neg/'+all_val_imgs[index])
            temp_data = resize(temp_data, img_size)
            temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
            temp_ratio = np.ravel(temp_ratio)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
            ratio.append(temp_ratio)
    return [np.array(x), np.array(ratio)], np.array(y)


# batch data generator - train
def generate_batch_train(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_train_data(batch, img_size)
                batch = []
                
# batch data generator - val
def generate_batch_val(idxs, img_size, batch_size = 16):
    batch = []
    while True:
        np.random.shuffle(idxs)
        for i in idxs:
            batch.append(i)
            if len(batch) == batch_size:
                yield load_val_data(batch, img_size)
                batch = []

# loading data function - based on val idxs
def load_unseen_data(batch_ids, img_size):
    x = []
    y = []
    ratio = []
    for index in batch_ids:
        if path.exists(data_path+'unseen/pos/'+all_unseen_imgs[index]):
            label_output = 1.0
            temp_data = tiff.imread(data_path+'unseen/pos/'+all_unseen_imgs[index])
            temp_data = resize(temp_data, img_size)
            temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
            temp_ratio = np.ravel(temp_ratio)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
            ratio.append(temp_ratio)
        elif path.exists(data_path+'unseen/neg/'+all_unseen_imgs[index]):
            label_output = 0.0
            temp_data = tiff.imread(data_path+'unseen/neg/'+all_unseen_imgs[index])
            temp_data = resize(temp_data, img_size)
            temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
            temp_ratio = np.ravel(temp_ratio)
            # temp_data = np.expand_dims(temp_data, axis = 0)
            x.append(temp_data)
            y.append(label_output)
            ratio.append(temp_ratio)
    return [np.array(x), np.array(ratio)], np.array(y)


# image data path
data_path = '/home/ubuntu/data/tiles/28 june all 13_for_cnn/'

# building model
K.clear_session()
model5 = classification_model5(13, (56, 56))
model5.summary()

# training model 5
model_callbacks5 = [EarlyStopping(patience=10, verbose=1),
                   ReduceLROnPlateau(factor=0.1, patience=3, min_lr=0.00001, verbose=1),
                   ModelCheckpoint(data_path+'model_5_weights.h5', monitor = 'val_loss', verbose=1, save_best_only=True, save_weights_only=True)]

model5.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

# loading train and valid data
all_train_imgs = listdir(data_path+'train/pos/') + listdir(data_path+'train/neg/')
train_idxs = np.array(range(len(all_train_imgs)))

# counting training samples and defining batch_size
num_samples = len(all_train_imgs)
train_batch = 32

all_val_imgs = listdir(data_path+'val/pos/') + listdir(data_path+'val/neg/')
val_idxs = np.array(range(len(all_val_imgs)))

# valid samples count
num_samples_val = len(all_val_imgs)

# creating a data generator object
train_generator = generate_batch_train(train_idxs, (56,56), batch_size = train_batch)
val_generator = generate_batch_val(val_idxs, (56,56), batch_size = num_samples_val)

# fitting model to test
model5.fit(train_generator, steps_per_epoch = ceil(num_samples / train_batch),  
           epochs=15, validation_data=val_generator, validation_steps=1, 
           callbacks = model_callbacks5)


## PREDICTIONS - Model 5

model5.load_weights(data_path+'model_5_weights.h5')

# unseen data
all_unseen_imgs = listdir(data_path+'unseen/pos/') + listdir(data_path+'unseen/neg/')
unseen_idxs = np.array(range(len(all_unseen_imgs)))

# training data predictions
train_x, train_y = load_train_data(np.array(range(len(all_train_imgs))), (56, 56))
train_pred5 = model5.predict(train_x)

# validation data predictions
valid_x, valid_y = load_val_data(np.array(range(len(all_val_imgs))), (56, 56))
valid_pred5 = model5.predict(valid_x)

# unseen data predictions
unseen_x, unseen_y = load_unseen_data(unseen_idxs, (56, 56))
unseen_pred5 = model5.predict(unseen_x)

tile_pred_unseen5 = classify_tile(all_unseen_imgs, unseen_pred5, unseen_y, pos_thresh = 0.35, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)
tile_pred_train5 = classify_tile(all_train_imgs, train_pred5, train_y, pos_thresh = 0.35, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)
tile_pred_valid5 = classify_tile(all_val_imgs, valid_pred5, valid_y, pos_thresh = 0.35, tile_thresh = 0.5).sort_values('Tile True Label', ascending = False).reset_index(drop=True)

results_model5 = pd.concat([tile_pred_train5, tile_pred_valid5, tile_pred_unseen5], axis=1)
results_model5.to_excel(data_path+'Model 5 - S35 T50.xlsx', index = False)


## 19KBB Data Scoring
# looped results
img_path = '/home/ubuntu/data/19KBB/'
excel_path = '/home/ubuntu/data/Excel_Results_Band/'

all_folders = listdir(img_path)
results = pd.DataFrame()
# code break on idx 65 of all_folders, continuing from 66
for foldr in all_folders:
    all_imgs = listdir(img_path+'/'+foldr)
    all_imgs = [img for img in all_imgs if len(img) > 72]
    if (len(all_imgs) > 10201) or (len(all_imgs) < 10200):
        continue
    col_names = [] 
    x = []
    band_ratio = []
    for img in all_imgs:
        temp_data = tiff.imread(img_path+'/'+foldr+'/'+img)
        temp_data = resize(temp_data, (56,56))
        file_name = img.split(',')[0] + '_' + img.split(',')[1].split('_')[1] + '_' + img.split(',')[1].split('_')[2] 
        col_names.append(file_name)
        x.append(temp_data)
        temp_ratio = np.divide(temp_data[:,:,3], temp_data[:,:,11], out=np.zeros_like(temp_data[:,:,3]), where=temp_data[:,:,11]!=0)
        temp_ratio = np.ravel(temp_ratio)
        band_ratio.append(temp_ratio)
    foldr_data = np.array(x)
    band_data = np.array(band_ratio)
    foldr_preds = model5.predict([foldr_data, band_data])
    temp_results = pd.DataFrame(np.transpose(foldr_preds), columns = col_names)
    temp_results['Folder_Name'] = foldr
    # temp_results.to_excel('check.xlsx', index = False)
    temp_results.to_excel(excel_path+foldr+'.xlsx', index = False)
    results = pd.concat([results, temp_results], axis = 0, sort = True)
    results.to_excel(data_path+'results_band.xlsx', index = False)
    print("Processed Folder:", foldr)




