#!/usr/bin/env python
# coding: utf-8

# In[1]:


## Pullng Data From S3 Bucket 
import boto3
import shutil
import os
# set the matplotlib backend so figures can be saved in the background
import matplotlib
get_ipython().run_line_magic('matplotlib', 'notebook')
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import cv2
import os
import imutils
from keras.optimizers import SGD
from keras.applications import VGG19
from keras.applications import ResNet50
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers import AveragePooling2D 
from keras.layers import Input
from keras.models import Model
import time
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from tensorflow.keras import regularizers
from keras.models import load_model
import multiprocessing 

import  import_ipynb
import model
from model import LeNet
import imutils
from sklearn.metrics  import log_loss
from sklearn.metrics  import confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
import seaborn as sns
def make_dir(new_path_train,new_path_val,new_path_unseen):
    if not os.path.exists(new_path_train):
        os.makedirs(new_path_train)
    else:
        shutil.rmtree(new_path_train)           # Removes all the subdirectories!
        os.makedirs(new_path_train)

    if not os.path.exists(new_path_val):
        os.makedirs(new_path_val)
    else:
        shutil.rmtree(new_path_val)           # Removes all the subdirectories!
        os.makedirs(new_path_val)

    if not os.path.exists(new_path_unseen):
        os.makedirs(new_path_unseen)
    else:
        shutil.rmtree(new_path_unseen)           # Removes all the subdirectories!
        os.makedirs(new_path_unseen)


# # Copying the data from S3 bucket to the instance 
# 

# In[ ]:


##Copying the data from S3 bucket to the instance
#aws s3 cp s3:'//rioml/tiles/19KBB 2016-01_2021-04/filtered_data/' '/home/ubuntu/data/tile/' --recursive
get_ipython().system("aws s3 cp s3:'//rioml/23 may training model B01+B10+B8A/' '/home/ubuntu/data/23may_data/' --recursive  ")


# # Combining the 13 tiff to form one tifff image

# In[ ]:





# In[ ]:





# In[ ]:


imagePaths1[0][12:17]+imagePaths1[0][-5:][0]


# In[ ]:


imPaths1=os.listdir(new_path+'/train/neg')
rowname=[i[12:17]+i[-5:][0] for i in imPaths1]
print(pd.value_counts(rowname))
imPaths1=os.listdir(new_path+'/train/pos')
rowname=[i[12:17]+i[-5:][0] for i in imPaths1]
pd.value_counts(rowname)


# In[2]:


## Splitting the dataset into train and validation 
def make_dir(new_path_train,new_path_val,new_path_unseen):
    if not os.path.exists(new_path_train):
        os.makedirs(new_path_train)
    else:
        shutil.rmtree(new_path_train)           # Removes all the subdirectories!
        os.makedirs(new_path_train)

    if not os.path.exists(new_path_val):
        os.makedirs(new_path_val)
    else:
        shutil.rmtree(new_path_val)           # Removes all the subdirectories!
        os.makedirs(new_path_val)

    if not os.path.exists(new_path_unseen):
        os.makedirs(new_path_unseen)
    else:
        shutil.rmtree(new_path_unseen)           # Removes all the subdirectories!
        os.makedirs(new_path_unseen)
        
#split_folders.ratio('/home/ubuntu/png data/', output="/home/ubuntu/png data valid", seed=1000, ratio=(0.8,0.2) ) # default value
path= '/home/ubuntu/data/tiles/13 june all 13'#please remove last slash "/"
new_path=path.rsplit('/',1)[0]+'/'+path.split('/')[-1]+'_for_cnn'
f=os.listdir(path+'/pos')
print(len(f))
print("positive-->",set([i[13:18] for i in f]))
print(len(set([i[13:18] for i in f])))
f=os.listdir(path+'/neg')
print(len(f))
print("negative-->",set([i[13:18] for i in f]))
print(len(set([i[13:18] for i in f])))
#6162+11612


# In[4]:



## Splitting data into training validation and unseen
data={'train_pos':[ '13RBJ', '19JDK', '19KES', '19JEG', '19JFN', '17MQS', '18KZG', '18NUG', '10TES', '10UCA', '19KDQ', '19JCJ', '10TFT', '19JGK', '22MFT', '18LTR', '19KER', '19JCH', '17PNK', '18QTE', '10UCG', '19JCG', '17NQA', '19HCT', '10UCE', '17PKL', '21LYJ', '17MPS', '19KEQ', '16TFT', '19QGA'],
    'val_pos':['10UCF', '19JDG', '19HCE', '21MWN','19JFH', '17MPR', '18NUN', '17PMK', '19JDM', '17PLK', '13SCC', '21MWM'],
    'unseen_pos':['19KBB', '19TDL', '18LTN', '19HDE'],

    'train_neg':['20HLC', '20HKK', '19JFM', '19JEH', '19HBB', '19HDV', '19KFB', '19HBS', '19HDS', '19JEJ', '19HDU', '19HGT', '19KGQ', '19HBU', '19JGM', '19KGP', '19JFK', '19HFB', '19GCK', '19HGE', '19GBP', '19HEC', '19HFA', '19HBT', '19KCQ', '19KGS', '19HDT', '19HED'],
    'val_neg':['19JEF', '19JFJ', '19GCH', '19GCQ', '19HEB', '19GCJ', '19GCM'],
    'unseen_neg':['19KEB', '19KDB', '19JEM', '19HGU']}

make_dir(new_path+'/train/pos',new_path+'/val/pos',new_path+'/unseen/pos')
make_dir(new_path+'/train/neg',new_path+'/val/neg',new_path+'/unseen/neg')

for i in data.keys():
    print(i)
    f=os.listdir(path+'/'+i.split('_')[1])
    files=[k for k in f if k[13:18] in data[i]]
    for fl in files:
        shutil.copy(path+'/'+i.split('_')[1]+'/'+fl, new_path+'/'+i.split('_')[0]+'/'+i.split('_')[1]+'/'+fl)
        
f=os.listdir(new_path+'/train/pos')
print(len(f))
f=os.listdir(new_path+'/train/neg')
print(len(f))

print('=======')
f=os.listdir(new_path+'/val/pos')
print(len(f))
#print("training positive-->",set([i[13:18] for i in f]))


f=os.listdir(new_path+'/val/neg')
print(len(f))
#print("training negative-->",set([i[13:18] for i in f]))


# In[ ]:





# In[4]:


f=os.listdir(new_path+'/train/pos')
print(len(f))
print("training positive-->",set([i[12:17] for i in f]))

f=os.listdir(new_path+'/train/neg')
print(len(f))
print("training negative-->",set([i[12:17] for i in f]))


# In[5]:


imPaths1=os.listdir(new_path+'/train/neg')
rowname=[i[13:18]+i[-5:][0] for i in imPaths1]
print(pd.value_counts(rowname))
imPaths1=os.listdir(new_path+'/train/pos')
rowname=[i[13:18]+i[-5:][0] for i in imPaths1]
pd.value_counts(rowname)


# In[2]:


path= '/home/ubuntu/data/June data'#please remove last slash "/"
new_path=path.rsplit('/',1)[0]+'/'+path.split('/')[-1]+'_for_cnn'


# In[6]:


train_path= new_path+'/train'
val_path  = new_path+'/val'
unseen_path  =new_path+'/unseen'
totalTrain = len(list(paths.list_images(train_path)))
totalVal = len(list(paths.list_images(val_path)))
totalUnseen = len(list(paths.list_images(unseen_path)))

print("TotalTrain length:",totalTrain)
print("Totalval length:",totalVal)
print("TotalUnseen length:",totalUnseen)
import gc
gc.collect()
train_path


# In[7]:


#Now we can easily fetch our train and validation data.
import keras
from keras.models import Sequential
from keras.layers import Dense, Conv2D , MaxPool2D , Flatten , Dropout ,BatchNormalization
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam

from sklearn.metrics import classification_report,confusion_matrix
from tifffile import imread, imwrite
from skimage.transform import resize
import tensorflow as tf
import numpy as np
labels = ['neg', 'pos']
img_size = 54#round(1830/2)
def get_data(data_dir):
    data = [] 
    for label in labels: 
        path = os.path.join(data_dir, label)
        class_num = labels.index(label)
        for img in os.listdir(path):
            try:
                img_arr = imread(os.path.join(path, img)) #convert BGR to RGB format
                resized_arr = resize(img_arr, (img_size, img_size,13),preserve_range=True) # Reshaping images to preferred size
                data.append([resized_arr, class_num])
            except Exception as e:
                print(e)
    return np.array(data)
# In[2]
train = get_data(train_path)
val = get_data(val_path)
unseen=get_data(unseen_path)
x_train = []
y_train = []
x_val = []
y_val = []
x_unseen = []
y_unseen = []

for feature, label in train:
  x_train.append(feature)
  y_train.append(label)

for feature, label in val:
  x_val.append(feature)
  y_val.append(label)
    
for feature, label in unseen:
  x_unseen.append(feature)
  y_unseen.append(label)


# In[ ]:





# In[9]:



# Normalize the data
x_train = np.array(x_train)# / 255
x_val = np.array(x_val)# / 255
x_unseen = np.array(x_unseen)# / 255

#x_train.reshape(-1, img_size, img_size, 1)
y_train = np.array(y_train)
y_val = np.array(y_val)
y_unseen = np.array(y_unseen)

model = Sequential()
#model.add(Dense(128,13), activation="relu")
model.add(Conv2D(256,13,padding="same", activation="relu", input_shape=(img_size,img_size,13)))
model.add(MaxPool2D(pool_size=(2,2)))

model.add(Conv2D(128, 13, padding="same", activation="relu"))
model.add(MaxPool2D())

model.add(Conv2D(64, 13, padding="same", activation="relu"))
model.add(MaxPool2D(pool_size=(2,2)))
model.add(Dropout(0.2))

#model.add(BatchNormalization)
#model.add(Conv2D(64, (3, 3), activation='relu', padding='same'))
model.add(Conv2D(128, (3, 3), activation='relu', padding='same'))
model.add(MaxPool2D(pool_size=(2, 2)))
model.add(Dense(256,activation="relu"))

model.add(Flatten())
model.add(Dropout(0.2))
#model.summary()
model.add(Dense(64,activation="relu"))
model.add(Dense(1, activation="sigmoid"))
opt = Adam(lr=0.00001)
from keras import backend as K


model.compile(optimizer = opt , loss = 'binary_crossentropy', metrics = ['accuracy'])
#Now, letâ€™s train our model for 500 epochs since our learning rate is very small.

history = model.fit(x_train,y_train,epochs = 25 ,batch_size=32, validation_data = (x_val, y_val))


# In[6]:


import joblib# save the model to disk
filename = 'finalized_model.h5'
joblib.dump(model, filename)


# In[10]:


x_unseen = np.array(x_unseen)
y_unseen = np.array(y_unseen)
probs=model.predict(x_unseen)


# In[8]:



probs=model.predict(x_val)
#pd.value_counts(y_val)/len(y_val)
print(probs.shape)
y_val.shape
max([i[0] for i in probs])


# In[37]:


from sklearn import metrics
#pred=[i[1] for i in probs]
predicted_class=[1 if i>0.03 else 0 for i in probs]
pd.value_counts(predicted_class)


# In[33]:


#predicted_class=[0 if i=='neg' else 1 for i in predicted ]
#pd.value_counts(y_unseen)
print(metrics.confusion_matrix(y_unseen,predicted_class))
print(metrics.accuracy_score(y_unseen,predicted_class))
print(metrics.classification_report(y_unseen,predicted_class))


# In[51]:


img_path=new_path+'/unseen'

flname1=os.listdir(img_path+'/neg')

flname2=os.listdir(img_path+'/pos')

imagePaths1=[img_path+'/neg/'+i for i in flname1]
imagePaths2=[img_path+'/pos/'+i for i in flname2]
imagePaths=imagePaths1+imagePaths2
rowname=[i.rsplit('/',1)[1][13:18]+i.rsplit('/',1)[1][-5:][0] for i in imagePaths]
pd.value_counts(rowname)
#imagePaths
#list(unseen_data_gen.classes)

#predicted_class=[1 if i>0.35 else 0 for i in probs]
pd.DataFrame([rowname,[i[0] for i in probs],[0] * len(flname1)+[1] * len(flname2)]).transpose().to_csv('/home/ubuntu/data/tiles/13bands_20k.csv')


# In[50]:


[0] * len(flname1[:2])+[1] * len(flname2[:2])


# In[ ]:


# function that helps us get tile name from crop image name
def get_tile_name(img_name):
    return img_name.split(',')[0]
# function for analyzing results at tile level
def classify_tile(pred_img_name, pred_prob, true_class, pos_thresh = 0.5, tile_thresh = 0.5):
    tile_name = list(map(get_tile_name, pred_img_name))
    data = pd.DataFrame(zip(tile_name, np.squeeze(pred_prob), np.squeeze(true_class)), columns = ['Tile Name', 'Pred Prob', 'True Class'])
    data['Pred Class'] = [1 if pred > pos_thresh else 0 for pred in pred_prob]
    grouped_res = data.groupby('Tile Name')['Pred Class', 'True Class'].mean().reset_index()
    grouped_res['Tile Pred'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['Pred Class']]
    grouped_res['Tile True Label'] = ['Positive' if pred > tile_thresh else 'Negative' for pred in grouped_res['True Class']]
    return grouped_res


# In[ ]:


## importing VGG16 and loading the weights of Image Net Data set:

baseModel=VGG19(weights='imagenet',include_top=False,input_tensor=Input(shape=(224, 224, 3)))

headmodel=baseModel.output
#headmodel=Dropout(0.5)(headmodel)
headmodel=Flatten(name="flatten")(headmodel)

## Applying regulariztion in dense layers to  prevent overfitting during the time of training of the head of the model

headmodel=Dense(256,activation="LeakyReLU")(headmodel)

headmodel=Dropout(0.3)(headmodel)


##Applying regulariztion in dense layers to  prevent overfitting during the time of training of the head of the model

headmodel=Dense(classes,activation="softmax")(headmodel)

Sentinel_model=Model(inputs=baseModel.input,outputs=headmodel)


# In[ ]:





# In[ ]:





# In[ ]:


## Freexing the top Base model layers  to prevent them from training at start
for layer in Sentinel_model.layers:
    layer.trainable = True
for layer in Sentinel_model.layers:
    print("{}: {}".format(layer, layer.trainable))
    
    
        


# In[ ]:


os.getcwd()


# In[ ]:


import PIL
gc.collect()
EPOCHS = 5
INIT_LR=1e-4
start = time.time()
PIL.Image.MAX_IMAGE_PIXELS = None
print("[INFO] compiling model...")
opt=Adam(learning_rate=INIT_LR,beta_1=0.9,beta_2=0.999,decay=INIT_LR / EPOCHS)
## ADAM WAS NOT WORKING WELL SO LETS TRY SGD !
Sentinel_model.compile(optimizer=opt,loss='binary_crossentropy',metrics=["accuracy"])
##es=EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
#mc=ModelCheckpoint('best_model_resnet.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
#filepathmodel = "0may_Sentinel_model-{epoch:02d}-{val_acc:.2f}.h5"
#mc_save = ModelCheckpoint(filepathmodel, monitor='val_acc', verbose=1, save_best_only=False, mode='max')
mc_save=ModelCheckpoint('23may_Sentinel_model--{epoch:02d}.h5', period=1,save_best_only=False)
print("[INFO] Training the Head of the model...")
H=Sentinel_model.fit_generator(train_data_gen,steps_per_epoch=totalTrain/batch_size,epochs=EPOCHS,
                    validation_data=val_data_gen,validation_steps=totalVal/batch_size,callbacks=[mc_save])


# In[ ]:


#k=Sentinel_model.predict(train_data_gen)
os.getcwd()
#model=load_model('Sentinel_model00000050.h5')


# In[8]:


## Pre processing the imput image Before fedding it to the model :
def pre_process(image_path):
    image=imread(image_path)
    #image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB) ## Converting the image into RGB format is the model was trained on RGB Fromat!
    image=resize(image, (54, 54,13))
    image=image.astype("float32")
    
   # mean = np.array([123.68, 116.779, 103.939], dtype="float32") 
   # image=image-mean 
    image=np.expand_dims(image, axis=0)
    return image


#model=load_model('20may_Sentinel_model--04.h5')#Sentinel_model#

##  label map is being generated from train_gen during training  time !!

label_map={'neg': 0,
 'pos': 1}
      
label_map =dict(map(reversed, label_map.items()))

def predict_image(image_path,model,topk=5):
        image=pre_process(image_path)
        pred=model.predict(image)[0]
        max_prob_class=np.argmax(pred)
        class_name=label_map[max_prob_class]
        topk_class=(-pred).argsort()[:topk]
        top_k_class_names=[label_map[i] for i in topk_class]
        return class_name,top_k_class_names,max_prob_class,pred

img_path=val_path
flname1=os.listdir(img_path+'/neg')
flname2=os.listdir(img_path+'/pos')
imagePaths1=[img_path+'/neg/'+i for i in flname1]
imagePaths2=[img_path+'/pos/'+i for i in flname2]
imagePaths=imagePaths1+imagePaths2
start = time.time()
predicted=[]
probs=[]
print(len(imagePaths))
for f in imagePaths:
    print(imagePaths.index(f))
    class_name,top_k_class_names,max_prob_class,prob=predict_image(f, model,topk=1)
    predicted.append(class_name)
    probs.append(prob)
    
print(f'Time: {time.time() - start}')


# In[15]:


max(pred)


# In[23]:


from sklearn import metrics
pred=[i[1] for i in probs]
predicted_class=[1 if i>0.00000000000001 else 0 for i in pred]
#predicted_class=[0 if i=='neg' else 1 for i in predicted ]
pd.value_counts(pred)
print(metrics.confusion_matrix(y_val,predicted_class))
print(metrics.accuracy_score(y_val,predicted_class))
print(metrics.classification_report(y_val,predicted_class))


# In[ ]:


predicted_class=[0 if i=='neg' else 1 for i in predicted ]
val_path
#train_data_gen.classes
#train_data_gen.classes
pred=[i[1] for i in probs]
#pd.value_counts(val_data_gen.classes)
pd.value_counts(pred)


# In[ ]:


#rowname=[i.rsplit('/',1)[1][:5] for i in imagePaths]
#imagePaths1=os.listdir(path+'/pos')
rowname=[i.rsplit('/',1)[1][12:17]+i.rsplit('/',1)[1][-5:][0] for i in imagePaths]
pd.value_counts(rowname)
#list(unseen_data_gen.classes)
predicted_class=[1 if i>0.7 else 0 for i in pred]
pd.DataFrame([rowname,predicted_class,list(val_data_gen.classes)]).transpose().to_csv('myfile_val_0.7.csv')


# In[ ]:


from sklearn import metrics
pred=[i[1] for i in probs]
predicted_class=[1 if i>0.6 else 0 for i in pred]
#predicted_class=[0 if i=='neg' else 1 for i in predicted ]
pd.value_counts(unseen_data_gen.classes)
print(metrics.confusion_matrix(unseen_data_gen.classes,predicted_class))
print(metrics.accuracy_score(unseen_data_gen.classes,predicted_class))
print(metrics.classification_report(unseen_data_gen.classes,predicted_class))
#predicted_class=[1 if i>0.7 else 0 for i in pred]


# In[ ]:


from sklearn import metrics
pred=[i[1] for i in probs]
predicted_class=[1 if i>0.6 else 0 for i in pred]
#predicted_class=[0 if i=='neg' else 1 for i in predicted ]
pd.value_counts(unseen_data_gen.classes)
print(metrics.confusion_matrix(val_data_gen.classes,predicted_class))
print(metrics.accuracy_score(val_data_gen.classes,predicted_class))
print(metrics.classification_report(val_data_gen.classes,predicted_class))
#predicted_class=[1 if i>0.7 else 0 for i in pred]


# In[ ]:


from tifffile import tifffile
image = tifffile.imread(r'/home/ubuntu/data/training Data/pos sample /19HDT,2016-01-22,1.tiff')
#print(image)
#tifffile.imwrite('my_image.tif', image, photometric='rgb')


# In[ ]:


from libtiff import TIFF
tif = TIFF.open(r'/home/ubuntu/data/3 band data/pos/21LYJ,2018-04-07,1.tiff', mode='r')
image = tif.read_image()
print(image.shape)
#@print(image)


# In[ ]:


im = cv2.imread('b.png')
im.shape


# In[ ]:


plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, 50), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, 50), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, 50), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, 50), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")


# In[ ]:


Sentinel_model=load_model("Sentinel_model00000050.h5",compile=True)


# In[ ]:


train_data_gen.reset()
val_data_gen.reset()


# In[ ]:


EPOCHS = 50
INIT_LR=1e-2
start = time.time()

print("[INFO] compiling model...")
opt=Adam(learning_rate=INIT_LR,beta_1=0.9,beta_2=0.999,decay=INIT_LR / EPOCHS)

## ADAM WAS NOT WORKING WELL SO LETS TRY SGD !

Sentinel_model.compile(optimizer=opt,loss='binary_crossentropy',metrics=["accuracy"])


##es=EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
#mc=ModelCheckpoint('best_model_resnet.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
mc_save=ModelCheckpoint('Sentinel_model{epoch:05d}.h5', period=10)



print("[INFO] Training the Head of the model...")
H=Sentinel_model.fit_generator(train_data_gen,steps_per_epoch=totalTrain/batch_size,epochs=EPOCHS,
                             validation_data=val_data_gen,validation_steps=totalVal/batch_size,callbacks=[mc_save],
                              workers=multiprocessing.cpu_count(),use_multiprocessing=True, shuffle=True) 

print(f'Time: {time.time() - start}')


# In[ ]:


## Pre processing the imput image Before fedding it to the model :
def pre_process(image_path):
    image=cv2.imread(image_path)
    image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB) ## Converting the image into RGB format is the model was trained on RGB Fromat!
    image=cv2.resize(image, (224, 224))
    image=image.astype("float32")
    
    #mean = np.array([123.68, 116.779, 103.939], dtype="float32") 
    #image=image-mean 
    image=np.expand_dims(image, axis=0)
    return image


model=load_model('Sentinel_model00000050.h5')

##  label map is being generated from train_gen during training  time !!
label_map={'Neg Sample': 0, 'Pos Sample': 1}
label_map =dict(map(reversed, label_map.items()))


def predict_image(image_path,model,topk=1):
        image=pre_process(image_path)
        pred=model.predict(image)[0]
        max_prob_class=np.argmax(pred)
        class_name=label_map[max_prob_class]
        topk_class=(-pred).argsort()[:topk]
        top_k_class_names=[label_map[i] for i in topk_class]
        return class_name,pred


# In[ ]:


classes=[]
for key,value in label_map.items():
    classes.append(value)
    


# In[ ]:


sample_paths=r"/home/ubuntu/Sentinel_Data_splitted/train"
imagePaths=sorted(list(paths.list_images(sample_paths)))
len(imagePaths)


# In[ ]:


start = time.time()
predicted=[]
predicted_prob=[]
for f in imagePaths:
    class_name,pred=predict_image(f, model,topk=1)
    predicted.append(class_name)
    predicted_prob.append(pred)
print(f'Time: {time.time() - start}')


# In[ ]:


actual=[]
for f in imagePaths:
    label= f.split(os.path.sep)[-2]
    actual.append(label)


# In[ ]:


accuracy_score(actual,predicted)*100


# In[ ]:


predicted_prob


# In[ ]:


cm=confusion_matrix(actual,predicted,labels=classes)
cm


# In[ ]:


cm_df = pd.DataFrame(cm,index=classes,columns=classes)   
plt.figure(figsize=(5,5))  
sns.heatmap(cm_df, annot=True,cmap="Blues")


# In[ ]:


image_path=r"/home/ubuntu/Sentinel_Data/model train/Pos Sample/19HCE,2021-01-28,B01-gdal_translate_0.5x0.5_cropped.tif"
image=cv2.imread(image_path)


# In[ ]:


image.shape


# In[ ]:


np.sum(image)


# In[ ]:




