#!/usr/bin/env python
# coding: utf-8

# In[12]:


## Pullng Data From S3 Bucket 
import boto3
import shutil
import os
# set the matplotlib backend so figures can be saved in the background
import matplotlib
get_ipython().run_line_magic('matplotlib', 'notebook')
from keras.preprocessing.image import ImageDataGenerator
from keras.optimizers import Adam
from sklearn.model_selection import train_test_split
from keras.preprocessing.image import img_to_array
from keras.utils import to_categorical
from imutils import paths
import matplotlib.pyplot as plt
import numpy as np
import argparse
import random
import cv2
import os
import imutils
from keras.optimizers import SGD
from keras.applications import VGG19
from keras.applications import ResNet50
from keras.layers.core import Dropout
from keras.layers.core import Flatten
from keras.layers.core import Dense
from keras.layers import AveragePooling2D 
from keras.layers import Input
from keras.models import Model
import time
from keras.callbacks import EarlyStopping
from keras.callbacks import ModelCheckpoint
from tensorflow.keras import regularizers
from keras.models import load_model
import multiprocessing 

import  import_ipynb
import model
from model import LeNet
import imutils
from sklearn.metrics  import log_loss
from sklearn.metrics  import confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
import seaborn as sns
def make_dir(new_path_train,new_path_val,new_path_unseen):
    if not os.path.exists(new_path_train):
        os.makedirs(new_path_train)
    else:
        shutil.rmtree(new_path_train)           # Removes all the subdirectories!
        os.makedirs(new_path_train)

    if not os.path.exists(new_path_val):
        os.makedirs(new_path_val)
    else:
        shutil.rmtree(new_path_val)           # Removes all the subdirectories!
        os.makedirs(new_path_val)

    if not os.path.exists(new_path_unseen):
        os.makedirs(new_path_unseen)
    else:
        shutil.rmtree(new_path_unseen)           # Removes all the subdirectories!
        os.makedirs(new_path_unseen)


# # Copying the data from S3 bucket to the instance 
# 

# In[13]:


##Copying the data from S3 bucket to the instance

get_ipython().system("aws s3 cp s3:'//rioml/extra png data/' '/home/ubuntu/data/png data final/' --recursive  ")


# # Combining the 3 tiff to form one tifff image

# In[ ]:





# In[ ]:





# In[ ]:





# In[ ]:





# In[13]:


## Splitting the dataset into train and validation 
import split_folders
#split_folders.ratio('/home/ubuntu/png data/', output="/home/ubuntu/png data valid", seed=1000, ratio=(0.8,0.2) ) # default value
path= '/home/ubuntu/data/png data final'#please remove last slash "/"
new_path=path.rsplit('/',1)[0]+'/'+path.split('/')[-1]+'_for_cnn1'
f=os.listdir(path+'/pos')
print(len(f))
print("positive-->",set([i[:5] for i in f]))

f=os.listdir(path+'/neg')
print(len(f))
print("negative-->",set([i[:5] for i in f]))


# In[ ]:


new_path


# In[14]:


## Splitting data into training validation and unseen
data={'train_pos':['19JFH', '19KEQ', '19JGK', '19JDM', '19JEG', '19KBB', '19KDQ', '21LYJ', '19JDG', '19JCH', '19JCJ', '22MFT', '21MWM'],
    'val_pos':['19KER', '19JDK'],
    'unseen_pos':['19JFN', '21MWN'],

    'train_neg':['19GCH', '19HBS', '19JFK', '19HBU', '19GCQ', '19JGM', '19GCK', '19HBT', '19JFM', '19KDB', '19HBB'],
    'val_neg':['19GCJ', '19KCQ'],
    'unseen_neg':['19GCM', '19GBP']}

make_dir(new_path+'/train/pos',new_path+'/val/pos',new_path+'/unseen/pos')
make_dir(new_path+'/train/neg',new_path+'/val/neg',new_path+'/unseen/neg')

for i in data.keys():
    f=os.listdir(path+'/'+i.split('_')[1])
    files=[k for k in f if k[:5] in data[i]]
    for fl in files:
        shutil.copy(path+'/'+i.split('_')[1]+'/'+fl, new_path+'/'+i.split('_')[0]+'/'+i.split('_')[1]+'/'+fl)
        
f=os.listdir(new_path+'/train/pos')
print(len(f))
print("training positive-->",set([i[:5] for i in f]))

f=os.listdir(new_path+'/train/neg')
print(len(f))
print("training negative-->",set([i[:5] for i in f]))


# In[15]:


train_path= new_path+'/train'
val_path  = new_path+'/val'
unseen_path  =new_path+'/unseen'
totalTrain = len(list(paths.list_images(train_path)))
totalVal = len(list(paths.list_images(val_path)))
totalUnseen = len(list(paths.list_images(unseen_path)))

print("TotalTrain length:",totalTrain)
print("Totalval length:",totalVal)
print("TotalUnseen length:",totalUnseen)
import gc
gc.collect()


# In[16]:


train_aug=ImageDataGenerator(  rotation_range=40,
                               zoom_range=0.2,
                               width_shift_range=0.2,
                                height_shift_range=0.2,
                                shear_range=0.15,
                                horizontal_flip=True,
                                fill_mode="nearest",
                            )

## We do not apply Data augmentstaion to valdiation or test setthats why we are passing it  no parameters
val_aug=ImageDataGenerator()
unseen_aug=ImageDataGenerator()

# Mean Values of RGB In IMAGE NET DATASET 
# We will use this mean to subtarcr from the input image for mean subtraction 
#mean=np.array([123.68, 116.779, 103.939], dtype="float32")
#train_aug.mean=mean
#val_aug.mean=mean

batch_size=16
classes=2


## Resnet 50 input shape is  224 *224
train_data_gen=train_aug.flow_from_directory(train_path,class_mode='categorical',target_size=(224,224)
                                             ,shuffle=True,batch_size=batch_size)

val_data_gen=val_aug.flow_from_directory(val_path,class_mode='categorical',target_size=(224,224),
                                         shuffle=True,batch_size=batch_size)  


unseen_data_gen=unseen_aug.flow_from_directory(unseen_path,class_mode='categorical',target_size=(224,224),
                                         shuffle=True,batch_size=batch_size)  


# In[ ]:


train_data_gen.class_indices 


# In[17]:


## importing VGG16 and loading the weights of Image Net Data set:

baseModel1=VGG19(weights='imagenet',include_top=False,input_tensor=Input(shape=(224, 224, 3)))

headmodel=baseModel1.output

headmodel=Flatten(name="flatten")(headmodel)

## Applying regulariztion in dense layers to  prevent overfitting during the time of training of the head of the model

headmodel=Dense(256,activation="relu")(headmodel)

headmodel=Dropout(0.5)(headmodel)


##Applying regulariztion in dense layers to  prevent overfitting during the time of training of the head of the model

headmodel=Dense(classes,activation="softmax")(headmodel)

Sentinel_model_v1=Model(inputs=baseModel.input,outputs=headmodel)


# In[11]:


baseModel1=VGG19(weights='imagenet',include_top=False,input_tensor=Input(shape=(224, 224, 3)))


# In[ ]:





# In[ ]:





# In[ ]:


## Freexing the top Base model layers  to prevent them from training at start
for layer in Sentinel_model.layers:
    layer.trainable = True
for layer in Sentinel_model.layers:
    print("{}: {}".format(layer, layer.trainable))
    
    
        


# In[ ]:


import PIL
gc.collect()
EPOCHS = 2
INIT_LR=1e-4
start = time.time()
PIL.Image.MAX_IMAGE_PIXELS = None
print("[INFO] compiling model...")
opt=Adam(learning_rate=INIT_LR,beta_1=0.9,beta_2=0.999,decay=INIT_LR / EPOCHS)
## ADAM WAS NOT WORKING WELL SO LETS TRY SGD !
Sentinel_model.compile(optimizer=opt,loss='binary_crossentropy',metrics=["accuracy"])
##es=EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
#mc=ModelCheckpoint('best_model_resnet.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
mc_save=ModelCheckpoint('Sentinel_model{epoch:08d}.h5', period=10,save_best_only=True)
print("[INFO] Training the Head of the model...")
H=Sentinel_model.fit_generator(train_data_gen,steps_per_epoch=totalTrain/batch_size,epochs=EPOCHS,
                    validation_data=val_data_gen,validation_steps=totalVal/batch_size,callbacks=[mc_save])


# In[ ]:


#k=Sentinel_model.predict(train_data_gen)
#os.getcwd()
#model=load_model('Sentinel_model00000050.h5')


# In[ ]:


## Pre processing the imput image Before fedding it to the model :
def pre_process(image_path):
    image=cv2.imread(image_path)
    image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB) ## Converting the image into RGB format is the model was trained on RGB Fromat!
    image=cv2.resize(image, (224, 224))
    image=image.astype("float32")
    
   # mean = np.array([123.68, 116.779, 103.939], dtype="float32") 
   # image=image-mean 
    image=np.expand_dims(image, axis=0)
    return image


#model=load_model('Sentinel_model00000020.h5')#Sentinel_model#
model=Sentinel_model#

##  label map is being generated from train_gen during training  time !!

label_map={'neg': 0,
 'pos': 1}
      
label_map =dict(map(reversed, label_map.items()))

def predict_image(image_path,model,topk=5):
        image=pre_process(image_path)
        pred=model.predict(image)[0]
        max_prob_class=np.argmax(pred)
        class_name=label_map[max_prob_class]
        topk_class=(-pred).argsort()[:topk]
        top_k_class_names=[label_map[i] for i in topk_class]
        return class_name,top_k_class_names,max_prob_class

img_path=unseen_path
flname1=os.listdir(img_path+'/neg')
flname2=os.listdir(img_path+'/pos')
imagePaths1=[img_path+'/neg/'+i for i in flname1]
imagePaths2=[img_path+'/pos/'+i for i in flname2]
imagePaths=imagePaths1+imagePaths2
start = time.time()
predicted=[]
print(len(image_path))
for f in imagePaths:
    print(imagePaths.index(f))
    class_name,top_k_class_names,max_prob_class=predict_image(f, model,topk=1)
    predicted.append(class_name)
print(f'Time: {time.time() - start}')


# In[ ]:


predicted_class=[0 if i=='neg' else 1 for i in predicted ]
pd.value_counts(predicted_class)
#train_data_gen.classes
#train_data_gen.classes


# In[ ]:


model.name


# In[ ]:


from sklearn import metrics
predicted_class=[0 if i=='neg' else 1 for i in predicted ]
pd.value_counts(unseen_data_gen.classes)
print(metrics.confusion_matrix(unseen_data_gen.classes,predicted_class))
print(metrics.accuracy_score(unseen_data_gen.classes,predicted_class))
print(metrics.classification_report(unseen_data_gen.classes,predicted_class))


# In[ ]:


from tifffile import tifffile
image = tifffile.imread(r'/home/ubuntu/data/training Data/pos sample /19HDT,2016-01-22,1.tiff')
#print(image)
#tifffile.imwrite('my_image.tif', image, photometric='rgb')


# In[ ]:


from libtiff import TIFF
tif = TIFF.open(r'/home/ubuntu/data/3 band data/pos/21LYJ,2018-04-07,1.tiff', mode='r')
image = tif.read_image()
print(image.shape)
#@print(image)


# In[ ]:


im = cv2.imread('b.png')
im.shape


# In[ ]:


plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, 50), H.history["loss"], label="train_loss")
plt.plot(np.arange(0, 50), H.history["val_loss"], label="val_loss")
plt.plot(np.arange(0, 50), H.history["accuracy"], label="train_acc")
plt.plot(np.arange(0, 50), H.history["val_accuracy"], label="val_acc")
plt.title("Training Loss and Accuracy")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="upper left")


# In[ ]:


Sentinel_model=load_model("Sentinel_model00000050.h5",compile=True)


# In[ ]:


train_data_gen.reset()
val_data_gen.reset()


# In[ ]:


EPOCHS = 50
INIT_LR=1e-2
start = time.time()

print("[INFO] compiling model...")
opt=Adam(learning_rate=INIT_LR,beta_1=0.9,beta_2=0.999,decay=INIT_LR / EPOCHS)

## ADAM WAS NOT WORKING WELL SO LETS TRY SGD !

Sentinel_model.compile(optimizer=opt,loss='binary_crossentropy',metrics=["accuracy"])


##es=EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
#mc=ModelCheckpoint('best_model_resnet.h5', monitor='val_accuracy', mode='max', verbose=1, save_best_only=True)
mc_save=ModelCheckpoint('Sentinel_model{epoch:05d}.h5', period=10)



print("[INFO] Training the Head of the model...")
H=Sentinel_model.fit_generator(train_data_gen,steps_per_epoch=totalTrain/batch_size,epochs=EPOCHS,
                             validation_data=val_data_gen,validation_steps=totalVal/batch_size,callbacks=[mc_save],
                              workers=multiprocessing.cpu_count(),use_multiprocessing=True, shuffle=True) 

print(f'Time: {time.time() - start}')


# In[ ]:


## Pre processing the imput image Before fedding it to the model :
def pre_process(image_path):
    image=cv2.imread(image_path)
    image=cv2.cvtColor(image,cv2.COLOR_BGR2RGB) ## Converting the image into RGB format is the model was trained on RGB Fromat!
    image=cv2.resize(image, (224, 224))
    image=image.astype("float32")
    
    #mean = np.array([123.68, 116.779, 103.939], dtype="float32") 
    #image=image-mean 
    image=np.expand_dims(image, axis=0)
    return image


model=load_model('Sentinel_model00000050.h5')

##  label map is being generated from train_gen during training  time !!
label_map={'Neg Sample': 0, 'Pos Sample': 1}
label_map =dict(map(reversed, label_map.items()))


def predict_image(image_path,model,topk=1):
        image=pre_process(image_path)
        pred=model.predict(image)[0]
        max_prob_class=np.argmax(pred)
        class_name=label_map[max_prob_class]
        topk_class=(-pred).argsort()[:topk]
        top_k_class_names=[label_map[i] for i in topk_class]
        return class_name,pred


# In[ ]:


classes=[]
for key,value in label_map.items():
    classes.append(value)
    


# In[ ]:


sample_paths=r"/home/ubuntu/Sentinel_Data_splitted/train"
imagePaths=sorted(list(paths.list_images(sample_paths)))
len(imagePaths)


# In[ ]:


start = time.time()
predicted=[]
predicted_prob=[]
for f in imagePaths:
    class_name,pred=predict_image(f, model,topk=1)
    predicted.append(class_name)
    predicted_prob.append(pred)
print(f'Time: {time.time() - start}')


# In[ ]:


actual=[]
for f in imagePaths:
    label= f.split(os.path.sep)[-2]
    actual.append(label)


# In[ ]:


accuracy_score(actual,predicted)*100


# In[ ]:


predicted_prob


# In[ ]:


cm=confusion_matrix(actual,predicted,labels=classes)
cm


# In[ ]:


cm_df = pd.DataFrame(cm,index=classes,columns=classes)   
plt.figure(figsize=(5,5))  
sns.heatmap(cm_df, annot=True,cmap="Blues")


# In[ ]:


image_path=r"/home/ubuntu/Sentinel_Data/model train/Pos Sample/19HCE,2021-01-28,B01-gdal_translate_0.5x0.5_cropped.tif"
image=cv2.imread(image_path)


# In[ ]:


image.shape


# In[ ]:


np.sum(image)


# In[ ]:




